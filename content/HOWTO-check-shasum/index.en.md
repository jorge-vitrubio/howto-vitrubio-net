---                                                                                                     
title: "Check file using shasum" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: [""]
category: [""]
 
lightgallery: true
---
 
```
echo "e739317677c2261ae746eee5f1f0662aa319ad0eff260d4eb7055d7c79d10952 *linuxmint-20.3-cinnamon-64bit.iso" | shasum -a 512 --check
```
