---                                                                                                     
title: "Nanostation atenna openwrt" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "Nanostation instalar software libremesh, openwrt para la red guifinet"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["nanostation","guifinet","libremesh","openwrt","antena","mesh"]
category: ["gnu/linux"]

lightgallery: true
---  

## Antenas Nanostation M5


### descargar Libremesh
veréis la versión que tenéis. Como han venido todas juntas
presumiblemente sea la misma (v5.6.2 (XW)).

Entonces desde esa versión HAY QUE HACER DOWNGRADE a la v5.5.X.

La versión se baja de aquí (considerad si teneis modelo XW o XM).

https://www.ubnt.com/download/airmax-m/nanostationm


```
usr: ubnt
pss: ubnt
```

En "See Past Firmware"

### instalar el firmware creado con chef

cambiar ip
```
ifconfig eth0 10.42.0.40
```
o con dhcp
```
dhclient -v eth0
```

#### si ya tiene libremesh la antena

bajar la imagen
comporobar el sha5
```
sisupgrade -n IMAGENAFLASHEAR
```

## gestión router nanostation

y con el navegador conectar a conectar a 
```
http://10.1.0.1/cgi-bin/luci
```
```
admin
pass: 13f
```
### Mesh network bmx gsf 
```
id nodo XXXXXXXX
id radio nodo XXXXXRd1
LAN publica nodo XX.XX.XX.XX
255.255.255.224
```
¿id mesh gracia GRABCNgsfRb5Rd1MESH?

### url de gestión del nodo
http://guifi.net/ca/guifi/device/90165/edit

http://dsg.ac.upc.edu/qmpgsf/index.php



entrar por ssh a la antena e introducir la ip en la config del router según documentado en:
```
vi /etc/config/lime
```
la configuracion
```
lime-config
```
https://github.com/libremesh/lime-packages/blob/develop/packages/lime-system/files/etc/config/lime-example

```
config lime network
#	option primary_interface eth0                                          # The mac address of this device will be used in different places
#	option bmx6_over_batman false                                          # Disables Bmx6 meshing on top of batman
	option main_ipv4_address '192.0.2.0/24'                                # Here you have 4 possibilities: set a static IP and the subnet, like '192.0.2.1/16'; parametrize with %Mn and %Nn, and set the subnet, like '192.%N1.%M5.%M6/16'; set a whole network address (so not a specific IP) for getting the IP autocompleted in that network with bits from MAC address, this works also with netmasks other than /24 or /16, like '192.0.128.0/17' (but not valid network addresses, for example '192.0.128.0/16' or '192.0.129.0/17', won't get parametrized); set two different parameters, the first for subnet and the second for IP parametrization, like '192.0.128.0/16/17', this results in /16 subnet but IP parametrized in a /17 range.
	option main_ipv6_address '2001:db8::%M5:%M6/64'                        # Parametrizable in the same way as main_ipv4_address. If used, the IP autocompletion will fill maximum the last 24 bits, so specifying an IP autocompletion range bigger than /104 is not useful.
	list protocols adhoc                                                   # List of protocols configured by LiMe, some of these require the relative package "lime-proto-...". Note that if you set here some protocols, you overwrite the *whole* list of protocols set in /etc/config/lime-defaults
	list protocols lan
	list protocols anygw
	list protocols batadv:%N1                                              # Parametrizable with %Nn (which depends from ap_ssid), note that this will range between 16 and 272
	list protocols bmx6:13
#	list protocols olsr:14
#	list protocols olsr6:15
#	list protocols olsr2:16
#	list resolvers 8.8.8.8                                                 # DNS servers node will use
#	list resolvers 2001:4860:4860::8844

```


## para quemar imagen por tftp
```
tftp <ip>
binary
trace on
put <bynary name>
quit
```


### para saber la ipv6

en el terminal

```
ping6 ff02::1%eth0
```

respondera con la ip

```
PING ff02::1%eth0(ff02::1%eth0) 56 data bytes
64 bytes from fe80::652e:5e8c:3d4d:2e97%eth0: icmp_seq=1 ttl=64 time=0.029 ms
64 bytes from fe80::46d9:e7ff:fed1:a60c%eth0: icmp_seq=1 ttl=64 time=0.564 ms (DUP!)
64 bytes from fe80::652e:5e8c:3d4d:2e97%eth0: icmp_seq=2 ttl=64 time=0.043 ms
64 bytes from fe80::46d9:e7ff:fed1:a60c%eth0: icmp_seq=2 ttl=64 time=0.485 ms (DUP!)
64 bytes from fe80::652e:5e8c:3d4d:2e97%eth0: icmp_seq=3 ttl=64 time=0.035 ms
64 bytes from fe80::46d9:e7ff:fed1:a60c%eth0: icmp_seq=3 ttl=64 time=0.473 ms (DUP!)
64 bytes from fe80::652e:5e8c:3d4d:2e97%eth0: icmp_seq=4 ttl=64 time=0.057 ms
64 bytes from fe80::46d9:e7ff:fed1:a60c%eth0: icmp_seq=4 ttl=64 time=0.474 ms (DUP!)

--- ff02::1%eth0 ping statistics ---
4 packets transmitted, 4 received, +4 duplicates, 0% packet loss, time 2997ms
rtt min/avg/max/mdev = 0.029/0.270/0.564/0.230 ms
```
