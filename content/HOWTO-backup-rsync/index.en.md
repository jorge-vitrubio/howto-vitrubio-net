---                                                                                                     
title: "Backup with rsync" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto install AppImage in linux"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["bash","rsync","backup","linux"]
category: ["gnu/linux"]
 
lightgallery: true
---
 
# rsync for backup files

This is an approach to do a bash scritp to backupfiles in *bash*

## print out dates
`date '+%Y_%m_%d'`

## backup files with proper permissions and groups
```
rsync -azxvAHX --numeric-ids /path/origin /path/dest
```
## backup excluding files and paths
```
rsync -azxvAHX --numeric-ids --exclude-from=/path/to/_rsync-exclude-files.txt /path/origin /path/destination
```
### the content of `_rsync-exclude-files.txt`

```
# we have to exclude from the original path of rsync
# in our case /home/
user1/
user2/
lost+found/
# exclude from any user
*/Downloads/
*/.atom/
*/.cache/
*/.mozilla/
*/.npm/
*/.local/share/TelegramDesktop/
*/.local/share/libvirt/images/
*/Pictures/20*

# specific files
*.iso
*wordpress*/*uploads*/
*wp*/*uploads*/

```
