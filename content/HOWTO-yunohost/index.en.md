---
title: "Yunohost server management"
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto install, configure and hack the Yunohost installs"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["yunohost","server","debian","php","sysadmin"]
category: ["gnu/linux"]
lightgallery: true
---

## yunohost web

### increase php upload limit

https://github.com/YunoHost-Apps/wordpress_ynh/issues/143

The **upload max limit** needs to be changed in two places 

#### change nginx 
`/etc/nginx/conf.d/<votredomaine>.tld.d`
change the `client_max_body_size 0;` value `0` bypasses the limit

#### change php-fpm

find the `fpm.conf` you are looking for: `ls -alF /etc/php/*/fpm/pool.d/`

then modify it: `vim /etc/php/<version>/fpm/pool.d/<application>.conf`

change value number and units `M` for megas or `G` for gigas

```txt
php_admin_value[upload_max_filesize] = <value>M
php_admin_value[memory_limit] = <value>M
php_admin_value[post_max_size] = <value>M
```
then restart the php service that was modified
```
yunohost service restart php<version>-fpm
```

## yunohost hacks

### debian backports

Since backports might conflict with the yunohost install we have to [pin]({{< ref "HOWTO-apt" >}}) them.

allow backports ins `sources.list`

`vim /etc/apt/sources.list.d/backports.list`

with this content

```txt
deb http://mirror.hetzner.de/debian/packages bullseye-backports main contrib non-free
```


create a preferences rule

`vim /etc/apt/preferences.d/bullseye_backports`

with this content

```txt
Package: *
Pin: release a=bullseye-backports
Pin-Priority: 100
```

## yunohost systemctl errors

```
yunohost service status
```

check failed in systemctl
```
# systemctl --failed
  UNIT               LOAD   ACTIVE SUB    DESCRIPTION
● user@55897.service loaded failed failed User Manager for UID 55897

LOAD   = Reflects whether the unit definition was properly loaded.
ACTIVE = The high-level unit activation state, i.e. generalization of SUB.
SUB    = The low-level unit activation state, values depend on unit type.
1 loaded units listed.
```
see the user name for UID
```
# id -nu <UID_NUMBER>
<user>
```

https://unix.stackexchange.com/questions/667937/debian-11-works-fine-but-strange-systemd-error-happen
```
# grep -i exec /lib/systemd/system/user@.service
ExecStart=/lib/systemd/systemd --user
```
as user
```
$ /lib/systemd/systemd --user --log-level=debug
systemd 247.3-7+deb11u4 running in user mode for user 55897/tech. (+PAM +AUDIT +SELINUX +IMA +APPARMOR +SMACK +SYSVINIT +UTMP +LIBCRYPTSETUP +GCRYPT +GNUTLS +ACL +XZ +LZ4 +ZSTD +SECCOMP +BLKID +ELFUTILS +KMOD +IDN2 -IDN +PCRE2 default-hierarchy=unified)
Failed to create '/run/user/55897/systemd/inaccessible', ignoring: File exists
Failed to create '/run/user/55897/systemd/inaccessible/reg', ignoring: File exists
Failed to create '/run/user/55897/systemd/inaccessible/dir', ignoring: File exists
Failed to create '/run/user/55897/systemd/inaccessible/fifo', ignoring: File exists
Failed to create '/run/user/55897/systemd/inaccessible/sock', ignoring: File exists
Failed to create '/run/user/55897/systemd/inaccessible/chr', ignoring: File exists
Failed to create '/run/user/55897/systemd/inaccessible/blk', ignoring: Operation not permitted
Found cgroup2 on /sys/fs/cgroup/, full unified hierarchy
Failed to determine root cgroup, ignoring cgroup memory limit: No such process
RLIMIT_MEMLOCK is already as high or higher than we need it, not bumping.
Found cgroup2 on /sys/fs/cgroup/, full unified hierarchy
Unified cgroup hierarchy is located at /sys/fs/cgroup/user.slice/user-55897.slice/session-12.scope.
Failed to create /user.slice/user-55897.slice/session-12.scope/init.scope control group: Permission denied
Failed to allocate manager object: Permission denied
```
solution applied:
```
# vim /etc/default/grub
```
add this `systemd.unified_cgroup_hierarchy=0` to file at `GRUB_CMDLINE_LINUX`
```
GRUB_CMDLINE_LINUX="apparmor=1 security=apparmor systemd.unified_cgroup_hierarchy=0"
```
and then apply changes
```
update-grub2
```
reboot

