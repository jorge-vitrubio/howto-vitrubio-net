---                                                                                                     
title: "Markdown Cheat Sheet" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "a markdown help for formatting to be used as a cheat sheet"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["example"]
category: [""]

lightgallery: true
---  

Thanks for visiting [The Markdown Guide](https://www.markdownguide.org)!

This Markdown cheat sheet provides a quick overview of all the Markdown syntax elements. It can’t cover every edge case, so if you need more information about any of these elements, refer to the reference guides for [basic syntax](https://www.markdownguide.org/basic-syntax/) and [extended syntax](https://www.markdownguide.org/extended-syntax/).

## Basic Syntax

These are the elements outlined in John Gruber’s original design document. All Markdown applications support these elements.

### Heading
```
# H1
```
# H1
```
## H2
```
## H2
```
### H3
```
### H3

### Bold

```
**bold text**
```
**bold text**

### Italic

```
*italicized text*
```
*italicized text*

### Blockquote

```
> blockquote
```
> blockquote

### Ordered List
```
1. First item
2. Second item
3. Third item
```
1. First item
2. Second item
3. Third item

### Unordered List
```
- First item
- Second item
- Third item
```

- First item
- Second item
- Third item

### Code
<code>`code`</code>

`code`

### Horizontal Rule
```
---
```
---

### Link
```
[Text to be linked](https://domain.tdl)
```
[Markdown Guide](https://www.markdownguide.org)

### Image
```
![alt text for image](http://domain.tdl/to/image.png)
![alt text for image](./path/to/image.png)
```
![alt text](https://www.markdownguide.org/assets/images/tux.png)

## Extended Syntax

These elements extend the basic syntax by adding additional features. Not all Markdown applications support these elements.

### Table
```
| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |
```
| Syntax | Description |
| ----------- | ----------- |
| Header | Title |
| Paragraph | Text |

### Fenced Code Block
<code>
```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```
</code>
```
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
```

### Footnote
```
Here's a sentence with a footnote. [^1]

[^1]: This is the footnote.
```
Here's a sentence with a footnote. [^1]

[^1]: This is the footnote.

### Heading ID
```
### My Great Heading {#custom-id}
```
### My Great Heading {#custom-id}

### Definition List
```
term
: definition
```
term
: definition

### Strikethrough
```
~~The world is flat.~~
```
~~The world is flat.~~

### Task List
```
- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media
```
- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media

### Emoji
```
That is so funny! :joy:
```
That is so funny! :joy:

(See also [Copying and Pasting Emoji](https://www.markdownguide.org/extended-syntax/#copying-and-pasting-emoji))

### Highlight
```
I need to highlight these ==very important words==.
```
I need to highlight these ==very important words==.

### Subscript
```
H~2~O
```
H~2~O

### Superscript
```
X^2^
```
X^2^
