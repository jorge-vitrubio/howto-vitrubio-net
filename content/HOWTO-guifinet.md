---                                                                                                     
title: "Guifinet web: como usarla" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "como usar la web de guifi.net"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["guifinet","guifi","mesh","wifi"]
category: ["gnu/linux"]

lightgallery: true
---  


más documentación: http://ca.wiki.guifi.net/wiki/Connectar-se_a_una_xarxa_mesh

# cómo configurar guifi.net en pocos pasos

visitar [guifi.net](http://guifi.net/)

## crear un nodo

hay que crear un nodo (dónde vamoas a tener la entena), para ello:

### creamos usuario/a en la web
http://guifi.net/user/register

(ojo el mail del user es semipúblico y relacionado con el nodo que tiene una ubicación geográfica precisa)

### crear contenido de la web

http://guifi.net/es/aportacions

y en concreto **crear un Nodo**
http://guifi.net/es/node/add/guifi-node

Campos obligatorios:
 -  Nombre de la Ubicación: CIUDADsubredCalleoPlazaNUMERO   (ex: BCNgsfTorrentOlla72)
 -  Funders (no necesario)
 -  Maintainers: tu user de guifi.net
 -  Nombre corto: CIUDADsubredCalleoPlazaNUMERO
 -  Informació del lugar: poner info del nodo, altura, lugar, que se ve y quizás un enlace a una galería de fotos de lo que se ve desde el techo.
 -  estado del nodo: reservado / inactivo / proyectado
 -  Contacto: el mail de contacto del nodo, normalmente el del user de guifi.net
 -  Zona: la zóna geográfica (barrio, red-mesh) del nodo (por ejemplo: 18189-Gràcia)
 -  Longitud / Latitud: 41.40057 / 2.15923
 -  descripción de la zona: (no necesario)
 -  Elevación de la antena: muy útil (aprox)

Pulsar  **guardar**

Crea una página web nueva, donde podremos poner las características de nuestro nodo:

Conviene **guardar** la URL (dirección) que nos de

http://guifi.net/es/node/XXXXXXX

por ejemplo en el caso del Ateneu Rosa de Foc: http://guifi.net/es/node/94356

### Añadir el trasto / cacharro que se conecta a la red:

en el nodo (ubicación física geográfica) tenemos que definir que aparato vamos a conectar a la red guifi.net (normalmente una antena wifi de 5Ghz)

Si es una antena elegimos: **"Dispositivo wireless, como  un router, AP..."**

Botón **añadir**

### Características del trasto / cacharro
 - Maintainer (no necesario)
 - Funders
    User: (necesario) Proveedor y comentario (no necesario)
 - Modelo de dispositivo, firmware y dirección MAC (necesario)

IMPORTANTE: click en "guardar y seguir modificando"

