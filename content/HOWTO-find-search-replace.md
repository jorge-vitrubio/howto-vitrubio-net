---                                                                                                     
title: "bash search, find, replace, sed, awk, grep" 
date: 2024-02-22
lastmod: 2024-02-22
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto find and search, then replace content in files"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["search","find","replace","grep","find","sed"]
category: ["gnu/linux"]

lightgallery: true
---  

## find

find files with name containing `foo`
`find -type f -name '*.foo'`

output with no line break
`find -type f -name '*.foo' -print0`

find not recusively, maximum depth
```
find -type f -name 'foo' -maxdepth 1
```

## grep

look for `foo` 
- `-r` recursively where `/path/`
- `-n` output filenames with line number
```
grep -nr -e 'foo' /path/
```

- `-l` output file names where `foo` is found
  `-Z` output filenames with no line break (like `-pint0`)
```
grep -rlZ 'foo' /path/
```
- `--include \*.bar` search for files wich name contains `*.bar`


## sed

replace `foo` with `bar` on `file`
```
sed -i 's/foo/bar/g file
```
same but keep and rename original file with `.bak`

```
sed -i.bak 's/foo/bar/g file
```

## concatenate 

- find `foo` recursively in `/path/` and replace it by `bar`
```
grep -rlZ 'foo' /paht/ | xargs -0 sed -i 's/foo/bar/g'
```

