---                                                                                                     
title: "Howto compress using tar, zip, ... " 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto use the packaging and compression tools like tar, zip, gzip, ... "
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["tar","zip","gzip","compress"]
category: ["gnu/linux"]

lightgallery: true
---  

## using tar

### compress
```
tar -czvf final.tar.gz origin/path/files
```

#### excluding files
exclude always before the source
```
tar --exclude='/path/to/excluded/file' -czvf destination.tar.gz /origin/path/files
```

### extract
```
tar -xzvf final.tar.gz
```
