---                                                                                                     
title: "Git, distributed version control" 
date: 2024-02-02
lastmod: 2024-02-12
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto use, set up and manage git"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["git","gitea",]
category: ["gnu/linux"]

lightgallery: true
---  

## Git howto
The most used websites for git howto's are:
- https://githowto.com/
- https://git-scm.com/

## set up a git server

the machine has to have `ssh` acces for `git` over `ssh`
the machine can have `http` server for `git` over `http`

in debian `apt install git`

check [Git on the server](https://git-scm.com/book/en/v2/Git-on-the-Server-The-Protocols)
### define origint

## upstreams
get and fork other projects using `git upstream` 
<https://stackoverflow.com/questions/50973048/forking-git-repository-from-github-to-gitlab#52954199>

say we want to fork/import another project int ours
```bash
git remote add upstream-gitlab-REPO https://gitlab.com/user/repository.git
git remote add upstream-github-REPO https://github.com/user/repository.git
```
then 
```bash
git fetch -all
```
and we can pull from any upstream 
```bash
git pull upstream-gitlab-REPO <BRANCH>
git pull upstream-github-REPO <BRANCH>
```

