---
title: "Debian apt for installing software"
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto use, manage and control the apt installing system in debian"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["debian","apt","installing","sources","repositories","pin"]
category: ["gnu/linux"]
lightgallery: true
---

## apt commands

## sources

located at

`/etc/apt/sources.list`

with debian repositories typically like
```
#------------------------------------------------------------------------------#
#                   OFFICIAL DEBIAN REPOS
#------------------------------------------------------------------------------#

###### Debian stable Main Repos
# deb http://deb.debian.org/debian/ stable main contrib non-free
# deb-src http://deb.debian.org/debian/ stable main contrib non-free

# deb http://deb.debian.org/debian/ stable-updates main contrib non-free
# deb-src http://deb.debian.org/debian/ stable-updates main contrib non-free

# deb http://deb.debian.org/debian-security stable/updates main
# deb-src http://deb.debian.org/debian-security stable/updates main

#------------------------------------------------------------------------------#
#                   OFFICIAL DEBIAN REPOS
#------------------------------------------------------------------------------#

####### Debian Main Repos
#
# Testing
deb http://deb.debian.org/debian/ testing main contrib non-free non-free-firmware

deb http://deb.debian.org/debian/ testing-updates main contrib non-free non-free-firmware

deb http://deb.debian.org/debian-security testing-security main

```

for extra repositories

`/etc/apt/sources.list.d/SOME-SOURCE.list`

content for example like this for **unstable** sources

```
#------------------------------------------------------------------------------#
#                   OFFICIAL DEBIAN REPOS
#------------------------------------------------------------------------------#

####### Debian Main Repos
#
# Unstable
deb http://deb.debian.org/debian/ unstable main contrib non-free non-free-firmware
```



### pining sources

Gives different priorities to how packages are installed. 

Look in `/etc/apt/preferences.d/` for pinning options and definitions

documentation: <https://linuxconfig.org/debian-pinning-howto> and <https://wiki.debian.org/AptConfiguration>

 - Priority \< 0: prevents the installation of a package version
 - \1 \<= Priority \<= 99: causes a version to be installed **only** if there is no installed version of the package.
 - \100 \<= Priority \<= 499: causes a version to be installed **unless** there is a version available belonging to some other distribution or the installed version is more recent
 - \500 \<= Priority \<= 989: causes a version to be installed **unless** there is a version available belonging to the target release or the installed version is more recent
 - \990 \<= Priority \<= 999: causes a version to be installed **even** if it does not come from the target release, **unless** the installed version is more recent
 - Priority \> 1000: causes a version to be installed **even if** this constitutes a downgrade of the package
 
```
Package: *
Pin: release a=stable
Pin-Priority: 900

Package: *
Pin: release a=testing
Pin-Priority: 600

Package: *
Pin: release a=unstable
Pin-Priority: 500
```

example pinning definition

`vim /etc/apt/preferences.d/50-debian-unstable`

with content like:

```
# track testing, but allow packages from oldstable, stable and unstable

Package: *
Pin: release l=Debian-Security
Pin-Priority: 1100

Package: *
Pin: release a=testing
Pin-Priority: 900

Package: *
Pin: release a=stable
Pin-Priority: 600

Package: *
Pin: release a=unstable
Pin-Priority: 50

Package: *
Pin: release a=oldstable
Pin-Priority: 1
```
