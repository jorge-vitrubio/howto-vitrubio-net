---
title: "Check space in disks and file systems"
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto check space available in file systems and disks"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["space","disks","filesystem","size","bash"]
category: ["gnu/linux"]
lightgallery: true
---

Several ways to know the free space in the file system

## bash

using *bash* commands

### files systems and disks

check the use, size and ocupations of files systems

 - `df -h`

### directories and paths

check the size of directories and/or paths

`du -hd 1 ./`

### sort directories by size

```
du -hd 1 <PATH-TO-CHECK> 2> /dev/null | sort -hr | tail -n +2 | head -<DEPTH-OF-DIRS>
```
#### making an exec bash script

create a file `sort-dir-by-size.sh` with this content

```
#!/bin/bash
#
# read at
# https://linuxconfig.org/list-all-directories-and-sort-by-size

# send errors to dev null
#    2> /dev/null
# du -hd 1 $PATH | sort -h 2> /dev/null
if [ $# != 2 ]; then
	echo "Incorrect number of arguments!" >&2
	echo "USAGE: ./sort-dir-by-size.sh [DIRECTORY] <number of dirs>"
fi
du -hd 1 $1 2> /dev/null | sort -hr | tail -n +2 | head -$2
```

give file exec permissions `ug+x sort-dir-by-size.sh`

exec it in bash `./sort-dir-by-size.sh`

### files

list files with size in human (K,M,G)

`ls -lh ./`

list all files with sizes in human

`ls -alFh ./`

#### sort files by size
```
find . -printf "%s\t %p\n" | sort -nr |more
```

#### find bigger files than 2k
```
find . -size +2k  -printf "%p \t%k kb\n" | sort -k2n
```

## Aplications

### using ``duf`` Disk Usage/Free Utility

Check the development at <https://github.com/muesli/duf>

#### install
<https://github.com/muesli/duf#installation>

Debian: `apt install duf`

#### usage

open terminal

jus type  `duf` wherever you are

with arguments like: `duf /home /some/path /some/file`

list everything `duf --all`
