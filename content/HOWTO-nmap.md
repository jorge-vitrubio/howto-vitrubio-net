# NMAP

## scan standard ports
```
nmap -n <IPofDOMAIN> 
```

## scan all ports
```
nmap -p0-65535 <IPofDOMAIN> 
```

### scan all UDP ports
```
nmap -sU <IPofDOMAIN> 
```

### scan all TCP ports
```
nmap -sT <IPofDOMAIN> 
```

### scan all TCP and UDP ports
```
nmap -sU -sT <IPofDOMAIN> 
```

## scan IP addresses
```
nmap -sS -Pn <IP> 
```

## scan all ports in safe mode
```
nmap --script "safe" -p- <IPorDOMAIN> 
```

## scan all ports in agresive mode
```
nmap --script "exploit" -p- <IPorDOMAIN> 
```

