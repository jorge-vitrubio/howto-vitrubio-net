---                                                                                                     
title: "Lismonk, a newsletter server" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto install, deploy and manage listmonk newsletters server"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["listmonk","newsletters","install","manage","postgresql","postgres"]
category: ["gnu/linux"]

lightgallery: true
---  

## home of Listmonk
https://listmonk.app/

## install
https://listmonk.app/docs/installation/

download latest release
https://github.com/knadh/listmonk/releases

### PostgreSQL

#### first install
```
apt install postgresql
```

#### become postgres user
```
su - postgres
```

#### enter postgres console
```
psql
```

#### create db
```
createdb DBNAME -l en_US.UTF-8 -0 postgres -E UTF8
```
#### create user for db
```
createuser --no-inherit --createdb DBNAME
createuser --superuser --no-inherit --no-createrole --no-createdb --pwprompt USERNAME
```

#### start using the db
```
postgres@newsletters:/var/www/listmonkonthefly$ psql
```

#### list databases
```
\l
```

#### connect to database
```
\c listmonkontheflydb
```

#### list definition terms of field
```
\dt
```

#### select subscriber_lists to be modified
```
SELECT * from subscriber_lists;
```
select 'unconfirmed' users;

```
SELECT * from subscriber_lists WHERE status='unconfirmed';
```

#### modify values
only one desired list using the `WHERE list_id=<number>` it will confirm all subscribers even _unsubscribed_ ones 
```
UPDATE subscriber_lists SET status='confirmed' WHERE list_id=1;
```
keep _unsubscribed_ status, modify only _unconfirmed_ suscribers in any list using the `WHERE status='unconfirmed`

```
UPDATE subscriber_lists SET status='confirmed' WHERE status='unconfirmed';
```

#### quit
```
\q

```

### upgrade
https://listmonk.app/docs/upgrade/

#### download & extract new

download latest linux from releases
https://github.com/knadh/listmonk/releases/

```
wget https://github.com/knadh/listmonk/releases/download/v2.3.0/listmonk_2.3.0_checksums.txt
wget https://github.com/knadh/listmonk/releases/download/v2.3.0/listmonk_2.3.0_linux_amd64.tar.gz

sha256sum listmonk*linux*tar.gz
cat listmonk*checksums.txt |grep linux

tar -xzvf listmonk_*_linux_amd64.tar.gz
```

backup the actual listmonk
```
tar -czvf listmonkhangar-bkp.tar.gz /var/www/listmonkhangar
```

stop the service
```
systemctl stop listmonkhangar.service
```

delete old files and replace by new ones
```
cd /var/www/listmonkhangar/
```
delete files but NOT uplodads
```
rm lismonk LICENSE README.md
tar -xzvf /root/listmonk_2.x.x_linux_amd64.tar.gz
chown -R www-data:www-data ./
./listmonk --upgrade
```
if everything is ok restart the service

```
systemctl start listmonkhangar.service
```
check service

```
systemctl status listmonkhangar.service
```

### export imort users

#### export from mailman
list_members <listname> > listname-members.txt

#### convert txt to csv
usin vim
```
cp listname-members.txt listname-members.csv
vim listname-members.csv
```
inside vim
```
:%s/\n/\,\r/g
```
in vim add `email,` to first line so it is the header of the CSV file.
duplicate file to generate names form begining of mail
in vim delete everything before `@`
https://stackoverflow.com/questions/24111723/delete-all-characters-after-in-each-line
```
:%norm f@C
```

in libreoffice merged both files
in vim added column at the end with `,{}`
```
:%s/\n/\,\{\}\r/g
```


#### upload to listmonk
apply changes to confirm users with postgresql commands

### avoid spam and maintain user lists

To avoid spam please use Captcha

#### clean up users

in Admin -> Settings -> Maintenance --->
 - suscripciones eliminar Huerfanas y Bloqueada
   en psql buscar la cadena 
   ```
   NOT EXISTS (SELECT 1 FROM subscriber_lists sl WHERE sl.subscriber_id = subscribers.id)
   ```

