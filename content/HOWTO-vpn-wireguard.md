# vpn wireguard

## wireguard client install
https://www.wireguard.com/install/

https://www.digitalocean.com/community/tutorials/how-to-set-up-wireguard-on-ubuntu-22-04

https://blogs.gnome.org/thaller/2019/03/15/wireguard-in-networkmanager/

## config files
place config files at `/etc/wireguard/`

then use `sudo wg-quick CONNECTION.conf`

## make config available no sudo

### wireguard with nmcli
https://www.cyberciti.biz/faq/how-to-import-wireguard-profile-using-nmcli-on-linux/

We can import `/etc/wireguard/wg0.conf` by typing the following command:

 - Set up shell environment variable: `file='/etc/wireguard/wg0.conf'`
 - Now import it using the nmcli command: `sudo nmcli connection import type wireguard file "$file"`
 - Rename profile `wg0` as `my-vpn`: `nmcli connection modify wg0 connection.id "my-vpn"`
 - Connect to the WG VPN by name: `nmcli connection up my-vpn`
 - Disconnect from the WG VPNe: `nmcli connection down my-vpn`

We can also import the `.conf` file like this:
`nmcli connection import type wireguard file "/path/to/wg0.conf"`


### breath

#### Creating the WireGuard Peer’s Key Pair
private key
```
wg genkey | sudo tee /etc/wireguard/private.key
WMCp7dcM/WlwTG2l2BNYsV4DZU92JsbA//Ynr6FGF1A=
```
public key
```
sudo cat /etc/wireguard/private.key | wg pubkey | sudo tee /etc/wireguard/public.key
OSqS3rhfaiokHM3aBmynJcfG3Y1ZeP5M/qAFTZRhhig=
```
#### Creating the WireGuard Peer’s Configuration File
```
sudo nano /etc/wireguard/wg0.conf
```
```
[Interface]
Address = 10.10.10.4/32,fd42::4/128
PrivateKey = yFSsPR56JchQmue826xfttrn+v1zH8WJD7COu8k011o=
DNS = 1.1.1.1,8.8.8.8
MTU = 1450

[Peer]
PublicKey = BWkXULz/z4nO3oXEYST26Tuer+lKvpgDZB7/AWCdVTQ=
PresharedKey = XVkd8b8G060iipYgE9g+FtmeTxSBeo3lh7xDVs/TqXI=
AllowedIPs = 0.0.0.0/0
Endpoint = 116.203.195.69:8095
PersistentKeepalive = 15
```



## server config

### limit traffic to wireguard ip's

Access your home network using WireGuard
https://emersonveenstra.net/blog/access-local-network-with-wireguard/

to apply `iptables` changes in web POst Up Script
```
iptables -t nat -A POSTROUTING -s 10.10.10.0/24 -o enp2s0 -j MASQUERADE 
```
so we don't share all connections through our wireguard but just access to the `10.10.10.0/24` network

```
[Interface]
Address = 10.10.10.1/24
ListenPort = 8096
PrivateKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
MTU = 1450
PostUp = iptables -t nat -A POSTROUTING -s 10.10.10.0/24 -o %i -j MASQUERADE;
PostDown = iptables -t nat -D POSTROUTING -s 10.10.10.0/24 -o %i -j MASQUERADE;
```

## client config

NAMING: **max 15 characters!!!** (explanation)[https://github.com/StreisandEffect/streisand/issues/1350#issuecomment-392770830]

### .conf example

#### access to all internet
route all traffic through the vpn

```                    
[Interface]            
Address = 10.10.10.253/32  
PrivateKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
DNS = 1.1.1.1,8.8.8.8  
                       
[Peer]                 
PublicKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
PresharedKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
AllowedIPs = 0.0.0.0/0
Endpoint = <serverIP>:<PORT>
PersistentKeepalive = 15   
```                    

#### restrict access to network ip's 10.10.10.0/24
```
[Interface]
Address = 10.10.10.253/32
PrivateKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
DNS = 1.1.1.1,8.8.8.8

[Peer]
PublicKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
PresharedKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
AllowedIPs = 10.10.10.0/24
Endpoint = <serverIP>:<PORT>
PersistentKeepalive = 15
```

#### restrict access to only one ip 10.10.10.1
```
[Interface]
Address = 10.10.10.2/32
PrivateKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
DNS = 1.1.1.1,8.8.8.8

[Peer]
PublicKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 
PresharedKey = xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
AllowedIPs = 10.10.10.1/32
Endpoint = <serverIP>:<PORT>
PersistentKeepalive = 15
```


## wireguard networking 

### access LAN from WAN (internet)

#### wireguard site to site vpn via public server - not all traffic goes through?
https://stackoverflow.com/questions/62934558/wireguard-site-to-site-vpn-via-public-server-not-all-traffic-goes-through

#### Access your home network from anywhere with WireGuard VPN
https://davidshomelab.com/access-your-home-network-from-anywhere-with-wireguard-vpn/

Make local resources available when connected to Wireguard VPN
https://unix.stackexchange.com/questions/638889/make-local-resources-available-when-connected-to-wireguard-vpn

## route vpn and ports iptables
https://www.ckn.io/blog/2017/11/14/wireguard-vpn-typical-setup/
