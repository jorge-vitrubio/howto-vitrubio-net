---                                                                                                     
title: "Docker commands cheatsheet"                                                                                               
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["docker","debian","linux"]
category: ["gnu/linux"]
         
lightgallery: true
---  

## install

- https://docs.docker.com/engine/install/debian/
- https://itsfoss.com/debian-install-docker/


### uninstall conflicts

conflicts with other docker.io packages
```
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
```

### Add Docker's official GPG key:
```
apt update
apt install ca-certificates curl gnupg
```
check if exists `ls -alF /usr/share/keyrings` else
```
install -m 0755 -d /usr/share/keyrings
```
download certificate and save it
```
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
chmod a+r /usr/share/keyrings/docker-archive-keyring.gpg
```

### Add the repository to Apt sources:
automatically add sources related to debian version:
```
echo \
   "deb [arch="$(dpkg --print-architecture)" signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian \
   "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
   sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```
but if it does not exist you can check into `/etc/apt/sources.list.d/docker.list` you can have this:

```
# debian 11
#deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.dockercom/linux/debian   bullseye stable
```
or
```
# debian 12
#deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/debian   bookworm stable
```

### install packages
```
apt update
apt install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin
```

### enable docker
```
systemctl enable --now docker
```

### install docker-compose

the version provided by debian is 1.29 and we need version 2
https://docs.docker.com/compose/install/
https://wiki.crowncloud.net/index.php?How_to_Install_and_use_Docker_Compose_on_Debian_12

no need to install it, just run
```
docker compose run hello-world
```

if still wanting to install it:
check if Docker Engine and Docker CLI are installed
```
apt list -a |grep docker |grep install
```
then install
```
sudo apt-get update
sudo apt-get install docker-compose-plugin
```
check version
```
docker compose version
```
### migrate to V2 from V1
https://docs.docker.com/compose/migrate/


### creatin group adn adding users

```
groupadd docker
```
add user
```
usermod -aG docker USERNAME
```
do not restar, login in the new group, with you UERNAME do
```
newgrp docker
```
check if it is working
```
docker run hello-world
```

if still does not work, jus reboot for changes to take effect

## configure
edit the config file
`vim docker-compose.yml`
with content (check at the bottom)

## list

running containers:
`docker ps -a`

list all containers:
`docker container ls -a`

list all downloaded images
`docker images ls`


## run
`docker run -OPTIONS`

with docker compose will use the `docker-compose.yml` file
`docker-compose up`

option `-d` to run as a daemon
`docker-compose up -d`

## start

`docker start CONTAINER_ID`

## stop

`docker stop CONTAINER_ID`

## remove

`docker rm CONTAINER_ID`

## update
get the latest image
`doker pull [docker_image]`
list images
`docker images`
list running dockers
`docker ps`
stop and delete dockers running old images
`docker stop [container_id]`

`docker start [container_id]`
remove old image
`docker image rm [docker_image]`
...or prune dangling images
`docker image prune`


## network

### list networks
`docker network ls`

### remove delete networks
remove one network
`docker network rm NETWORK`

remove all unsed networks
`docker network prune`

## volumes
https://docs.docker.com/engine/reference/commandline/volume/

### mount docker volumes
- list volumes  `docker volume ls` 
- mount volumes  `docker run -v /path/in/host:/path/in/container image-name....`


## debug logs
to see logs from inside a running docker

```
docker logs -f CONTAINER-NAME > /dev/null
```

## free up space

### check space

`docker system df`
or
`docker system df -v`

### prune unused containers

`docker system prune`

better prune all unused data
`docker system prune -a --volumes`

### stored images

`docker images -a` lists all images


#### remove images and/or containers
`docker rm <CONTAINER ID>`

`docker image rm wordpress:5.9.3-php7.4-fpm`

#### prune images
`docker image prune`

more agressive
`docker image prune -a`

## accesing inside docker containers

### exec bash into container

`docker exec -it <container_name> bash -l`


### attach into container

#### check running container
`docker ps`

#### attach to running container
`sudo docker attach container_Name`

example `sudo docker attach testhangar-wp-59-db-localdkr`

once here you can exec commands into docker

### ssh inside container

#### get the docker ip
`sudo docker inspect -f "{{ .NetworkSettings.IPAddress }}" <container_name>`


for older docker engines
`docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' xxx.xxx.xxx.xxx`
you'll get the name and ip of all running dockers

check the docker is there with a ping
`ping –c 3 xxx.xxx.xxx.xxx`

#### ssh into it
`ssh root@xxx.xxx.xxx.xxx`


## Building containers

Howto build docker containers.

### files needed

#### using a prebuild image
create a `docker-compose.yml`
```
version: '3.1'
services:
  web:
    image: odoo:16.0
    build:
      dockerfile: Dockerfile
      context: .
    container_name: odoo-modified
    ...

```
#### building our own image
create a `Dockerfile`

```
# https://github.com/odoo/docker/issues/257
#

FROM odoo:16.0
# Of course, you can change the 12.0 by the version you want

# If you have to install from pip and using Odoo >= 11.0
#RUN python3 -m pip install WHAT_YOU_NEED
# for https://apps.odoo.com/apps/modules/11.0/base_dav/
RUN python3 -m pip install radicale

# add env PATH
# https://docs.docker.com/engine/reference/builder/#environment-replacement
# https://docs.docker.com/develop/develop-images/dockerfile_best-practices/#env
ENV PATH=/var/lib/odoo/.local/bin:$PATH

# If you have to install from pip and using Odoo < 10.0
#RUN python -m pip install WHAT_YOU_NEED

# If you need to install something with apt, if not working try to add apt-get update
# RUN apt-get update
#RUN apt-get install PACKAGE_NAMES
```

### commands

Then build the image and run containers

 - only builds the images, does not start the containers:
 
    `docker-compose build`

 - builds the images if the images do not exist and starts the containers:
 
    `docker-compose up`

 - forcing to build the images even when not needed:

     `docker-compose up --build`

 - skiping the image build process:

     `docker-compose up --no-build`


## Specific apps yml files

### WordPress
check the [HOWTO WordPress Docker and migrating]({{< ref "HOWTO-wp-docker" >}})
