# samba
https://unixcop.com/how-to-configure-samba-server-with-debian-11/
https://pimylifeup.com/raspberry-pi-samba/

## intalling
check the networks and 
```
root@oficina ~ # ip -c r
default via xxx.xxx.xxx.xx3 dev enp2s0 onlink 
yyy.yyy.yyy.0/24 dev wg0 proto kernel scope link src yyy.yyy.yyy.yy1 
xxx.xxx.xxx.xx2/27 via xxx.xxx.xxx.xx3 dev enp2s0 
xxx.xxx.xxx.xx2/27 dev enp2s0 proto kernel scope link src xxx.xxx.xxx.x42 
```
we have two networks:
  - `xxx.xxx.xxx.xx3` internet ip on the `xxx.xxx.xxx.xx2/27` network
  - `yyy.yyy.yyy.yy1` vpn wireguard ip on the `yyy.yyy.yyy.yy0/24` network

install 
```
apt update
apt install samba
```
enable samba on reboot
```
systemctl enable smbd
```

##securing samba
https://www.rigacci.org/docs/biblio/online/Samba-HOWTO-Collection/securing-samba.html

### using yunohost iptables
**yunohost** follow this: https://forum.yunohost.org/t/how-to-turn-yunohost-into-a-nas-with-samba/18034

*yunohost* [firewall](https://forum.yunohost.org/t/yunohost-firewall-command-line-examples/12780/2) extend using iptables
```
vim /etc/yunohost/hooks.d/post_iptable_rules/my_custom_rules
```
https://stackoverflow.com/a/7423408
https://ixnfo.com/en/iptables-rules-for-samba.html
```
sudo iptables -A INPUT -s 0.0.0.0/0 -p udp -m udp --dport 137 -j DROP
sudo iptables -A INPUT -s 0.0.0.0/0 -p udp -m udp --dport 138 -j DROP
sudo iptables -A INPUT -s 0.0.0.0/0 -p tcp -m tcp --dport 139 -j DROP
sudo iptables -A INPUT -s 0.0.0.0/0 -p tcp -m tcp --dport 445 -j DROP
sudo iptables -A INPUT -s 10.22.10.0/24 -p udp -m udp --dport 137 -j ACCEPT
sudo iptables -A INPUT -s 10.22.10.0/24 -p udp -m udp --dport 138 -j ACCEPT
sudo iptables -A INPUT -s 10.22.10.0/24 -p tcp -m tcp --dport 139 -j ACCEPT
sudo iptables -A INPUT -s 10.22.10.0/24 -p tcp -m tcp --dport 445 -j ACCEPT
```
check the rules applied

```
sudo iptables -nvL
```

### using ufw
if firewall with ufw do
```
sudo ufw allow proto udp to 192.168.1.60 port 137 from 192.168.1.0/24
sudo ufw allow proto udp to 192.168.1.60 port 138 from 192.168.1.0/24
sudo ufw allow proto tcp to 192.168.1.60 port 139 from 192.168.1.0/24
sudo ufw allow proto tcp to 192.168.1.60 port 445 from 192.168.1.0/24
```
configure and use samba
https://hexadix.com/set-shared-folder-using-samba-ubuntu-auto-mount/
```
sudo vim /etc/samba/smb.conf
```
restart service
```
sudo service smbd restart
testparm
```
or also
```
sudo systemctl restart smbd.service
```
## restrict sharing with networks
https://www.samba.org/samba/docs/server_security.html
https://unix.stackexchange.com/a/613409

if wireguard is on network `10.8.0.0` then
```
ip a
```
we need to add wireguard ip as interface
https://www.samba.org/samba/docs/current/man-html/smb.conf.5.html#INTERFACES
```
interfaces = lo eth0 10.8.0.0/24
```
### check parameters
```
testparm
```

## Users and groups

### Groups
#### list system groups
```
less /etc/groups
```
#### create group
```
groupadd sambashare
```
#### list members of `sambashare` group
```
getent grop sambashare
```
#### add user to group
```
usermod -aG sambashare <name>
```
### Users
#### List users
```
pdbedit -L -v
```
#### add user

has to be a linux user `useradd <name>`
better doit with out acces to the operating system, home or login
```
sudo adduser --no-create-home --disabled-password --disabled-login <name>
```

then add it to samba
```
smbpasswd -a <name>
```
#### modify user
```
smbpasswd -a <name>
```
#### deleting user
```
smbpasswd -x <name>
```

then delete from system
```
userdel -r <name>
```
# Samba client
mount samba file systems

```
apt install cifs-utils
```
