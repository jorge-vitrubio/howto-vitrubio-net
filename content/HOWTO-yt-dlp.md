---                                                                                                     
title: "yt-dlp dowload audio and video" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto use yt-dlp to download videos and audios from websites"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["dowload","video","audio","youtube","bandcamp"]
category: ["gnu/linux"]

lightgallery: true
---  


from the developer website:
https://github.com/yt-dlp/yt-dlp

## format examples
https://github.com/yt-dlp/yt-dlp#format-selection-examples

to check all available formats
```
yt-dlp -v -F
```
Download the quatity wanted, ex: mp3  at max quality
```
yt-dlp -v -f mp3
```

Download the smallest video available
```
$ yt-dlp -S "+size,+br"
```

