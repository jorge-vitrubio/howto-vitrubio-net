# OpenWRT HOWTO

## Jailing IoT devices with OpenWRT
http://blog.sergem.net/jailing-iot-devices-with-openwrt/

Creating a Separate Network with OpenWRT

### Create an additional wifi network on one of the radios
Login into OpenWRT, go to Network-> WiFi in main menu, click Add
In Interface Configuration, General Settings, specify SSID for new wifi network, check “create” as a network connection and fill its name (I called it “lanpw”):

### Set wifi security for it
On Wireless Security tab, specify wifi encryption and password:
Click “Save”.

### Assign it an IP address
Now go to Network-> Interfaces, find the interface you’ve just created (“lanpw” in my case)

Set protocol to “Static Address”  and click “Change Protocol”

Fill in an IP address (this also defines the address of the network, so make sure to specify something different from other networks you have. I chose 192.168.5.1, with netmask 255.255.255.0 the network is 192.168.5.0) :

### Enable DHCP server in the network
Specify the DHCP network range – click “Setup DHCP Server”, I left the defaults:

### Allow it to communicate with WAN
on Firewall Settings tab, select “unspecified -or- create:” and fill name for new firewall zone.
Click “Save”

Connect the created network to the Internet (WAN):

Select Network-> Firewall in the main menu and find your created firewall zone, “lanpw” in my case, and click Edit:
Check connections to wan for source and destination forwarding. Note I left out the connection to my lan, as I don’t want my new network to communicate with it.

Click “Save and Apply” and restart the router. Test the new network with a mobile phone or some other device. It will be able to talk to the Internet, but not devices in the home network.


## Clamping Upload Bandwidth

Create a traffic control class in QoS settings restricted to 5kbps
Mark traffic from the jailed network with this class in firewall scripts

Building on the (Simple QoS)[http://blog.sergem.net/openwrt-qos-a-simple-recipe/] settings I run, I added a new class, and named it `"1:40"` :

```
$TC class add dev $IF_DSL parent 1:1 classid 1:40 hfsc ls rate 5kbit ul rate 5kbit
```
Ls 5kb means minimum limit when other traffic is present and ul 5kb means the limit when there is no other traffic.

In the script, add classification command – make sure to edit your interface name and IP address:
```
$IPTMOD -s 192.168.5.0/24 -j CLASSIFY --set-class 1:40
```
I also add same command in OpenWRT firewall custom rules, reloaded each time firewall config changes, this is the same command as above but all variables expanded:
```
/usr/sbin/iptables -t mangle -A POSTROUTING -o eth0 -s 192.168.5.0/24 -j CLASSIFY --set-class 1:40
```
