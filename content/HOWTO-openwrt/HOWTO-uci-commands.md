# uci commands
https://openwrt.org/docs/guide-user/base-system/uci

## uci show config
all config `uci show`

just one thing: `uci show <watever>.<other>.<something>`

all network settings `uci show network`
network lan setting `uci show network.lan`
network wan dns `uci show network.wan.dns`

## uci set config
```
uci set <watever>.<other>.<something>=<set.this.value>
```
for example
```
uci set network.wan.dns='1.1.1.1 8.8.8.8'
```

## Uci networking cheatsheet
https://openwrt.org/docs/guide-user/network/ucicheatsheet

## Openwrt as access point / dumb access point / dumb ap
https://openwrt.org/docs/guide-user/network/wifi/dumbap

## OpenWrt as router device
https://openwrt.org/docs/guide-user/network/openwrt_as_routerdevice

uci set network.lan.ipaddr=new-ip-address

### change WAN properties
```
uci set network.wan.proto=static
uci set network.wan.ipaddr=new-wan-ip-address
uci set network.wan.gateway=new-gateway-ip-address
uci set network.wan.dns=dns-ip-address
```

### apply changes 
```
uci commit
service network restart
```

## change ip

### edit config
```
vim /etc/config/network
```

restart networking
```
/etc/init.d/network restart
```

### use uci commands
```
uci set network.lan.ipaddr='192.168.1.5'
uci commit network
service network restart
```

## disable dhcp
https://openwrt.org/docs/guide-user/network/wifi/dumbap#step_3disable_dhcp_server

### to keep dnsmasq for TFTP
```
uci set dhcp.lan.ignore=1
uci commit dhcp
/etc/init.d/dnsmasq restart
```
### to completely disable
Option A:
```
/etc/init.d/dnsmasq disable
/etc/init.d/dnsmasq stop
```
Option B:
```
uci set network.lan.ipaddr="192.168.1.5"
uci set network.lan.gateway="192.168.1.1"
uci set network.lan.dns="192.168.1.1"
uci commit
/etc/init.d/dnsmasq disable
/etc/init.d/dnsmasq stop
/etc/init.d/network restart
```

## packages opkg

```
opkg update
```

https://adminswerk.de/openwrt-opkg-update-all/

### check upgradable packages in tmux
```
tmux new -s upgradesystem "opkg update && opkg list-upgradable| awk '{print $1}'| tr '\n' ' '| xargs -r opkg upgrade"
```

---

## Torify access
this is interesting because always will have an exit, no matter what happens to de ipv4 or ipv6 network

values stored in:
```
vi /etc/tor/torrc
```

### HiddenServiceDir /var/lib/tor/hidden_service/
```
HiddenServicePort 22 127.0.0.1:22¡
```

restart tor to get an `.onionaddress` assignated

```
/etc/init.d/tor restart
```
#### the .onionaddress
then check the value on the file `"hostname"`, this will be your `.onionaddress`
```
cat /var/lib/tor/hidden_service/hostname
```
#### save parameters permanently

changes in `/var` are **not** persistent, save them in `/etc/tor` :

```
cp -r /var/lib/tor/hidden-sevices /etc/tor/
```

after content is in `/etc/tor/hidden-services/` now we can modify `/etc/tor/torrc` 

 - change this `HiddenServiceDir /var/lib/tor/hidden_service/ `
 - to this `HiddenServiceDir /etc/tor/hidden_service/`

## Access using tor
To access using tor by ssh just do:

`torify ssh root@XXX.onion`
