---                                                                                                     
title: "Installing Kodi and OSMC into RaspBerryPi 3" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto install kodi, osmc, mediacente in raspberry raspi 3"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["kodi","osmc","raspi","raspberry","mediacenter"]
category: ["gnu/linux"]

lightgallery: true
---  

## Raspy3 install kodi
https://discourse.osmc.tv/t/howto-put-osmc-on-sd-card-manually-in-linux/10142

### prerequisites
 Linux, packages gdisk, md5sum, gzip

### download
https://osmc.tv/download/

### check sum
``` 
md5sum -c - <<<"175b87851dbf1143df351cfb4134dbdd OSMC_TGT_rbp2_20220314.img.gz"
```

### with sd card
#### check
```
sudo lsblk
```
Next you put in your SD card and check if it got automatically mounted with lsblk. 

#### umount
```
sudo umount /dev/sdx1
```
If it is, you should unmount the corresponding file system first with umount /dev/sdx1.


#### double check!!!

Because you want to be sure you leave no traces of a partition table (WARNING: you can destroy your data! 

Double check if it’s the right disk with first!
```
fdisk -l
```

you do a quick
```
gdisk /dev/sdx
``` 
(without the partition number!) and zap the GPT with `x`, `z` and two times `y`.

Then you’re finally able to transfer those SD card installation files onto the card:
(WARNING: you can destroy your data! Double check the right disk with `fdisk -l` first!)

```
gzip -dc OSMC_TGT_rbp2_20220314.img.gz | pv | sudo dd=/dev/sdx && sync
```
