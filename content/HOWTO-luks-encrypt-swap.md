---                                                                                                     
title: "Using LUKS for full disk encryption" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto full disk and swap encryption with temporary keys"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["example"]
category: [""]

lightgallery: true
---  

from https://superuser.com/questions/1498151/full-disk-encryption-and-encrypting-swap-with-temporary-keys

## full disk encryption and encrypting swap with temporary keys

```
# lsblk -f
NAME               FSTYPE FSVER  LABEL UUID                                   FSAVAIL FSUSE% MOUNTPOINTS
sda
├─sda1             vfat   FAT32        XXXX-XXXX                               505.1M     1% /boot/efi
├─sda2             ext2   1.0          XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX      300M    29% /boot
└─sda3             crypto 2            XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
  └─sda3_crypt     LVM2_m LVM2 0       XXXXX-XXXX-XXXX-XXXX-XXXX-XXXX-XXXXX
    ├─computer--vg-root
    │              ext4   1.0          XXXXXXXX-XXXX-XXXX-XXXXX-XXXXXXXXXXXX      5.9G    73% /
    ├─computer--vg-swap_1
    │              swap   1            XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX                  [SWAP]
    └─computer--vg-home
                   ext4   1.0          XXXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX     14.7G    77% /home
```



A simple write-up of what I ended up doing. As mentioned before, the distribution is Debian Buster, and hibernation is disabled.

 - Optional: Disable swap (`sudo swapoff -a`) and overwrite the swap volume with zeroes or with random data. This is actually recommended, but was not necessary in my case.

 - Create the new swap file
```
sudo fallocate -l 2G /cryptswap
```

 - Only root user should be able to read the swap file
```
sudo chmod 600 /cryptswap
```

 - Add the following line to `/etc/crypttab`

```
cryptswap /cryptswap /dev/urandom cipher=aes-cbc-essiv:sha256,size=256,swap,noearly
```

    Note that not all distributions support the noearly option, but Debian certainly does, and Ubuntu seems to support it as well.

 - Disable the swap mount in /etc/fstab (add a # in front of the line) and paste the second line from below:
```
#/dev/mapper/HOST--vg-swap_1 none swap sw 0 0
/dev/mapper/cryptswap none swap sw 0 0
```
 - [ADDED] Disable resuming from hibernation. Update `/etc/initramfs-tools/conf.d/resume` like below and run `sudo update-initramfs -u -k all` afterwards.
```
RESUME=none
```
 - Reboot

Confirm everything works as it should

Note that the old swap volume still exists, but is not mounted any more.
```
# lsblk -f
NAME                    FSTYPE      FSVER    LABEL     UUID                                   FSAVAIL FSUSE% MOUNTPOINTS
loop0                                                                                                        
└─cryptswap             swap        1        cryptswap XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX                  [SWAP]
sda                                                                                                          
├─sda1                  vfat        FAT32              XXXX-XXXX                               505.1M     1% /boot/efi
├─sda2                  ext2        1.0                XXXXXXXX-ad6a-4a81-ad00-857176a5820c    299.9M    29% /boot
└─sda3                  crypto_LUKS 2                  XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX                
  └─sda3_crypt          LVM2_member LVM2 001           XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX                
    ├─computer--vg-root   ext4        1.0                XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX    3.9G    81% /
    ├─computer--vg-swap_1 swap        1                  XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX                
    └─computer--vg-home   ext4        1.0                XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX   14.7G    77% /home
sdb                                                                         
```
```
# cryptsetup status cryptswap 
/dev/mapper/cryptswap is active and is in use.
  type:    PLAIN
  cipher:  aes-cbc-essiv:sha256
  keysize: 256 bits
  key location: dm-crypt
  device:  /dev/loop0
  loop:    /cryptswap
  sector size:  512
  offset:  0 sectors
  size:    4194304 sectors
  mode:    read/write
```
 - Cleanup
   Remove the old swap volume. Subsequently you may want to reassign the free space, but I won't cover this here.

```
sudo lvremove HOST-vg/swap_1
```


