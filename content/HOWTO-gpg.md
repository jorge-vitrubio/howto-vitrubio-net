---                                                                                                     
title: "GPG encryption and key management" 
date: 2024-10-15
lastmod: 2024-10-15
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto use gpg and manage keys"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["gpg","keys","criptography","security"]
category: ["linux"]

lightgallery: true
---  

This HowTo will try to follow you in basic use of gpg commands and the keyfile management.

## Receive key
```
gpt --receive-keys <HASH>
```

## List keys
```
gpg --list-keys
```

## Search Key
```
gpg --list-keys | grep email@account.org
gpg --list-keys | grep <HASH>
```
