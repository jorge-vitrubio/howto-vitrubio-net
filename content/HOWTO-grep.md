---                                                                                                     
title: "grep files" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto grep files to find content"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["grep","search"]
category: ["gnu/linux"]

lightgallery: true
---  

## find content

look for 'foo'
```
grep -r 'foo' /path/where/to/look
```

look for 'foo' in files finishing in '.bar'
```
grep -r --include="*.bar" 'foo' /path/where/to/look
```

look for excactly 'foo' in files and show where it appears
```
grep -nrw /path/whre/to/look -e 'foo'
```
