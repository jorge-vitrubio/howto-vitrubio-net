# rsycn

## backup files with permissions
```
rsync -azxvAHX --numeric-ids /original/path/to/copy/. /destination/path/to/copy/.
```

## backup excluding files
```
rsync -azxvAHX --numeric-ids --exclude-from=/root/_rsync-exclude-files.txt /path/what/to/backup/ /path/where/to/backup/
```

```
rsync -avz --numeric-ids --exclude-from=_rsync-exclude-files.txt /path/what/to/backup/ /path/where/to/backup/
```


## resume copy and get info
`--partial` : do not remove copied chunks even if copy is not finished

```
rsync -v --progress --stats -h -h --partial -e ssh user@server:/remote/path/to/copy /local/path/
```

```
rsync -v --progress --stats -h -h --partial hangar-liante-internet:/home/jorge/bigfile /home/vitrubio/hangar/bigfile
```
