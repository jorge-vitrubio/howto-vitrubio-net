# System

## unattended upgrades
https://pimylifeup.com/unattended-upgrades-debian-ubuntu/

## DynDNS client
`apt install ddclient`
`vim /etc/ddclient.conf`
https://joker.com/faq/index.php?action=artikel&cat=11&id=427&artlang=en&highlight=dyndns

## Public IP
https://ripe.net
https://svc.joker.com/nic/myip
https://svc.joker.com/nic/checkip

## Lets Encrypt
https://janw.me/raspberry-pi/install-lets-encrypt-ssl-on-raspberry-pi/

## Fail2Ban
https://www.linuxcapable.com/how-to-install-configure-fail2ban-on-debian-11/
https://pimylifeup.com/raspberry-pi-fail2ban/

## Firewall UFW
https://pimylifeup.com/raspberry-pi-ufw/
https://pimylifeup.com/configuring-ufw/


`apt install ufw`
`sudo ufw allow PORT`
`sudo ufw deny PORT`
`sudo ufw limit PORT`
`sudo ufw show added`


## wireguard vpn server
https://www.smarthomebeginner.com/linux-wireguard-vpn-server-setup/
works on UDP

### install and config wireguard server
```
apt install wireguard-tools
apt install wireguard
cd /etc/wireguard
umask 077
wg genkey > server.key
wg pubkey < server.key > server.pub
vim /etc/wireguard/wg0.conf
```

```
[Interface]
Address # 10.20.30.1/24
ListenPort # 49312
SaveConfig # True
```

`echo "PrivateKey # $(cat server.key)" >> /etc/wireguard/wg0.conf`


### run wireguard server
bring up the server
`sudo wg-quick up wg0`

enable at systemboot
`sudo systemctl enable wg-quick@wg0`

status
`sudo wg show`


### open ports firewall for vpn

`sudo ufw allow 49312/udp`

### create client config

#### as client user in vpn server
```
cd ~
umask 077
mkdir wireguard
cd wireguard/
mkdir suit-laptop
cd suit-laptop/
wg genkey > suit-laptop.key
wg pubkey < suit-laptop.key > suit-laptop.pub
wg genpsk > suit-laptop.psk
vim suit-laptop.conf
```

```
[Interface]
Address # 10.20.30.40/32
```
```
echo "PrivateKey # $(cat suit-laptop.key)" >> suit-laptop.conf
echo "[Peer]" >> suit-laptop.conf 
echo "Endpoint # 192.168.1.60:49312" >> suit-laptop.conf 
echo "AllowedIPs # 10.20.30.0/24" >> suit-laptop.conf 
echo "PublicKey # $(sudo cat /etc/wireguard/server.pub)" >> suit-laptop.conf 
echo "PresharedKey # $(cat suit-laptop.psk)" >> suit-laptop.conf 
```

#### as root server in vpn server
```
sudo -i
cd /etc/wireguard
echo "[Peer]" >> wg0.conf 
echo "AllowedIPs # 10.20.30.40/32" >> wg0.conf 
echo "PublicKey - $(cat /home/chisme/wireguard/suit-laptop/suit-laptop.pub)" >> wg0.conf 
echo "PresharedKey # $(cat /home/chisme/wireguard/suit-laptop/suit-laptop.psk)" >> wg0.conf 
```
```
wg-quick down wg0
wg-quick up wg0
```
```
[soft reset]: wg syncconf wg0 <(wg-quick strip wg0) 
```

# Mount points /etc/fstab


https://www.raspberrypi.com/documentation/computers/configuration.html#external-storage-configuration
## adding volumes
run to see partitions and mountpoints

```
sudo lsblk -o UUID,NAME,FSTYPE,SIZE,MOUNTPOINT,LABEL,MODEL
```
run to see the blkid
```
sudo blkid
```
### easy but too loose way of mounting
```
#UUID=16E1-2F3E /mnt/MEDIATECA  vfat    defaults,auto,users,rw,nofail,umask=000 0       0
```
### more secure way
https://unix.stackexchange.com/a/521072
https://askubuntu.com/a/429858
 - Xmask=ugo u:user - g:group - o:others
 - umask the defaul user mounting and permissions given
 - dmask directory mask  dmask=0000
 - fmask file mask       fmask=0111
 - id <username> -> will give the UID and GID
 - uid=<mounting username>
 - guid=<mounting groupname>

the mask permissions:
```
    0   1   2   3   4   5   6   7
r   +   +   +   +   -   -   -   -
w   +   +   -   -   +   +   -   -
x   +   -   +   -   +   -   +   -
```

```
UUID=16E1-2F3E  /mnt/MEDIATECA  vfat    defaults,auto,user,rw,nofail,uid=1002,gid=1002,fmask=0113,dmask=0002    0       0
```

# File share
## samba
https://unixcop.com/how-to-configure-sambadd-server-with-debian-11/
https://pimylifeup.com/raspberry-pi-samba/
securing samba
https://www.rigacci.org/docs/biblio/online/Samba-HOWTO-Collection/securing-samba.html

if firewall with ufw do
```
sudo ufw allow proto udp to 192.168.1.60 port 137 from 192.168.1.0/24
sudo ufw allow proto udp to 192.168.1.60 port 138 from 192.168.1.0/24
sudo ufw allow proto tcp to 192.168.1.60 port 139 from 192.168.1.0/24
sudo ufw allow proto tcp to 192.168.1.60 port 445 from 192.168.1.0/24
```
configure and use samba 
https://hexadix.com/set-shared-folder-using-samba-ubuntu-auto-mount/
```
sudo vim /etc/samba/smb.conf
sudo service smbd restart 
testparm 
```

## docker

### run docker as non root
```
sudo groupadd docker
```
answer should be `groupadd: group 'docker' already exists`

```
sudo usermod -aG [non-root-user]
```
## docker commands

bring up a docker in daemon mode
```
docker-compose up -d
```
list running dockers
```
docker ps
```
list all containers
```
docker container ls -a
```
prune stoped containers
```
docker container prune
```
list all downloaded images
```
docker images ls
```
show disk usage
```
docker system df
docker system df -v
```
remove all unused data
```
docker system prune -a --volumes
```
## phyton pip

list installed packages
`pip list`

removed uninstall packages
`pip uninstall PACKAGENAME`

https://stackoverflow.com/questions/7915998/does-uninstalling-a-package-with-pip-also-remove-the-dependent-packages
```
# install pip-autoremove
pip install pip-autoremove
# remove "somepackage" plus its dependencies:
pip-autoremove somepackage -y
```

## nextcloud
https://nextcloud.com/install/#instructions-server

nextcloud comunity docker (lighter)
https://hub.docker.com/_/nextcloud/ __

```
docker pull nextcloud
```

we choose the standard apache based image and then we rely on nginx proxy:
```
docker run -d nextcloud
```

to mount MEDIATECA into docker:
https://github.com/nextcloud/docker/issues/236#issuecomment-453417782
```
 volumes:
      - nextcloud:/var/www/html
      - /media/mydrive/4TB/nextcloud:/var/www/html/data:rw <---------- my nextcloud config is also on my mounted drive.
      - /media/mydrive/4TB/Media:/media/4TB/Media:rw <------------ actual mounted drive
```

To use the Nextcloud command-line interface (aka. occ command):
```
docker exec --user www-data CONTAINER_ID php occ
```
or for docker-compose:

```
docker-compose exec --user www-data app php occ`
```
docker examples for dockerfiles and docker-compose
https://github.com/nextcloud/docker/tree/master/.examples

### docker installation FPM vesion
https://hub.docker.com/_/nextcloud/  __
full readme: https://github.com/docker-library/docs/blob/master/nextcloud/README.md

# Multimedia

## navidrome

https://hub.docker.com/r/deluan/navidrome

upgrade version:
```
docker pull deluan/navidrome

docker-compose up -d
```

## kodi
https://kodi.wiki/view/HOW-TO:Install_Kodi_on_Raspberry_Pi
https://petermolnar.net/article/lightweight-headless-media-player-raspberry-pi/

## hi-fi player
https://www.runeaudio.com/

## moode audio player
https://moodeaudio.org/
https://github.com/moode-player/moode

## Mopidy (not installed) 
extensible music server
https://mopidy.com/
https://docs.mopidy.com/en/latest/installation/raspberrypi/

## Music Player Daemon **
https://www.musicpd.org/
installing in raspbian
https://kaliko.me/debian/
sudo apt install -t bullseye-backports mpd

configure
https://mattscodecave.com/posts/setting-up-raspberry-pi-and-mpd-music-player-daemon-with-raspbian.html

volumne config
https://nocin.eu/raspberrypi-installing-mpd-on-a-raspberrypi-3-with-hifiberry-amp2/

### headless mpd
outdated https://www.lesbonscomptes.com/pages/raspmpd.html

### clients for mpd
https://www.musicpd.org/clients/

## Distros

### archphile
distro for ArchlinuxARM/MPD/myMPD
https://archphile.org/

### pi musicbox
distro for pi using mopidy
https://www.pimusicbox.com/

## download

### Transmission (installed)
/home/chisme/torrent/
 
### Aria2 (not installed)i
https://aria2.github.io/
https://wiki.archlinux.org/title/Aria2

apt get install aria2

set it as daemon
vim /etc/aria2.daemon
vim /lib/systemd/user/aria2c.service

## SoulSeek / Nicotine
oficial nicotine
  https://nicotine-plus.org/
 pip packaged
  https://nicotine-plus.org/doc/DOWNLOADS.html#other-distributions

not supported in debian/raspbian
  https://forums.raspberrypi.com/viewtopic.php?t=323559

dockerized
  https://github.com/kokmok/rpi-nicotine-novnc/blob/master/Dockerfile

other opinions:
  https://www.reddit.com/r/Soulseek/comments/bb6hi5/tutorial_how_to_install_soulseek_on_raspberry_pi/

### slskd dockerized

La documentación de esa imagen:
https://github.com/slskd/slskd

```
version: "2"
services:
  slskd:
    image: slskd/slskd:latest
    container_name: slskd
    ports:
      - "0.0.0.0:15000:5030"
      - "15001:5001"
      - "60000:50000"
    environment:
      - PUID=1001
      - PGID=1001
    volumes:
      - ./data:/app
      - ./music:/media/music
      #- /mnt/MEDIATECA/music:/media/music
    restart: always
```
  
Antes de arrancarlo por primera vez:
```
$ mkdir data music
$ chmod 777 data music
```
Y luego ya:
```
$ docker-compose pull
$ docker-compose up -d
```
Y si quieres mirar los logs:
```
$ docker-compose logs -f
```
Y luego puedes entrar al soulseek via web:
```
http://localhost:15000
```

### writing a fork of ofiginal soulseek-docker noVNC
original and more developed docker:
  https://github.com/realies/soulseek-docker

fork at:
https://github.com/jorge-vitrubio/soulseek-docker-rpi/blob/rpi-arm-v7

apply this changes:
https://blog.alexellis.io/getting-started-with-docker-on-raspberry-pi/

to 
https://github.com/jorge-vitrubio/soulseek-docker-rpi/blob/rpi-arm-v7/Dockerfile
