---                                                                                                     
title: "Ninx web server" 
date: 2024-11-12
lastmod: 2024-11-12
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto use and configure nginx options"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["web server,nginx"]
category: [""]

lightgallery: true
---  

(Nginx)[https://nginx.org/en/] is an HTTP server.
The official documentation: https://nginx.org/en/docs/

## Redirecting
https://www.digitalocean.com/community/tutorials/nginx-rewrite-url-rules

### Temporary redirect 302

#### Rewriting the URL
```
server {
    . . .
    server_name www.domain1.com;
    rewrite ^/$ http://www.domain2.com redirect;
    . . .
}
```

### Permanent redirect 301

#### Rewriting the URL
The rules are
```
rewrite ^/$ http://www.domain2.com permanent;
rewrite ^/(.*)$ http://www.domain2.com/$1 permanent;
```
the config in `/etc/nginx/sites-available/domain1.com`
```
server {
    . . .
    server_name domain1.com;
    rewrite ^/(.*)$ http://domain2.com/$1 permanent;
    . . .
}
```
