---                                                                                                     
title: "Howto install windows 10" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto install a clean windows 10"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["install","windows","windows 10"]
category: ["Howto"]

lightgallery: true
---  

## Reinstalling Windows 10

### howto read here
https://answers.microsoft.com/en-us/windows/forum/all/clean-install-windows-10-on-new-hdd/ea44534d-7c75-469b-adf6-45d18a481b0d


Hi Lynn, you can reinstall Windows 10 at any time and it will not cost you anything !

Click this link:
https://www.microsoft.com/en-us/software-download/windows10

to download the Media Creation Tool (Click on Download Tool Now), with that you can download the latest Windows 10 ISO (Select Create Installation Media for Another PC), you can create a bootable USB flash drive (min 8GB) using that tool

Then connect that flash drive to your PC, Boot your PC from the Installation Media you just created (change Boot Order in your BIOS) to begin installing Windows 10

Since you previously had Windows 10 installed and activated on that PC during the installation process skip the steps which ask for a product key and select the option 'I am Reinstalling Windows 10 on this PC', and activation will not be an issue, your PC will have a digital entitlement stored on the Microsoft Activation Servers

If you have problems booting from a boot disc, you may have UEFI BIOS:

Insert the Bootable Installation Media, then go into your BIOS and make the following changes:
 1. Disable Secure Boot
 2. Enable Legacy Boot
 3. If Available enable CSM
 4. If Required enable USB Boot
 5. Move the device with the bootable disc to the top of the boot order
 6. Save BIOS changes, restart your System and it should boot from the Installation Media
