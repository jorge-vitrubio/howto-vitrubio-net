---                                                                                                     
title: "Backup with borg" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto consistently do backups with borg"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["backup","files","disks","borg"]
category: ["gnu/linux"]

lightgallery: true
---  


## Borg Backup
https://borgbackup.readthedocs.io/en/stable/

https://borgbackup.readthedocs.io/en/stable/quickstart.html
### initializa backup
```
borg init --encryption=repokey /path/to/repo
```


### Create backup
```
borg create -v --list /path/to/borgbackup::BORGBACKUPNAME /path/to/origin --exclude '/home/*/.cache' --exclude '.cache/*' --exclude '*.tmp'
```
```
borg create --stats --progress --compression zlib /path/to/borgbackup::BORGBACKUPNAME /path/to/origin
```
will ask for a passphrase, create one and write it down securely

### Bash script
```
#!/bin/sh

#remote backup
#REPOSITORY=username@ipx.xxx.xxx.xxx:/path/to/borgbackup
#local backup
REPOSITORY=/path/to/borgbackup

borg create -v --list $REPOSITORY::{user}--{now:%Y-%m-%d} \
            /boot                                       \
            /etc                                        \
            /root                                       \
            /var                                        \
            --exclude '/home/*/.cache'                  \
            --exclude '.cache/*'                        \
            --exclude '*.tmp'

# Use the `prune` subcommand to maintain 7 daily, 4 weekly and 6 monthly
# archives of THIS machine. The '{hostname}-' prefix is very important to
# limit prune's operation to this machine's archives and not apply to
# other machine's archives also.
borg prune -v $REPOSITORY --prefix root-- \
            --keep-daily=7 --keep-weekly=4 --keep-monthly=6
```



## applied to Hetzner

[Hetzner Borg Backup](https://community.hetzner.com/tutorials/install-and-configure-borgbackup)

[Hetzner storagebox borg](https://docs.hetzner.com/robot/storage-box/access/access-ssh-rsync-borg/)


### create ssh id_ed25519 keys

#### in the client
  (the server that we want to backup)

```
ssh-keygen -o -a 512 -t ed25519 -C "$(hostname)-$(date +'%d-%m-%Y')"
```
give name and no need for password, has to run from host in cron job

well have two keys:
  - private `id_ed25519` remains in our server never give away
  - public `id_ed25519.pub` we can give to have acces granted

#### in the host
  (server where to save backup)
copy the content of public key into `~/.ssh/authorized_keys`

change permissions
`chmod 700 ~/.ssh`
`chmod 600 ~/.ssh/authorized_keys`

(try the key doing ssh into this server with the key 
`ssh -p<port> -i .ssh/id_ed25519 <user>@<server-ip-or-url>` 

if it works, now we have access with no password to the backupserver.

### Install Borg and configure enviroment

#### install Borg
```
apt update
apt install borgbackup
```
if using yunohost
```
yunohost tools update
yunohost install borg
```

### configure borg
follow setup and will create id_ed25519 for borg and send you an email.
```
ssh://username@your.domain.url:23/./path/to/backups/
```

### run borg as service
to run a bakcup just launch it
```
systemctl start borg
```


## backup server : ssh + external storage

yunohost help
https://forum.yunohost.org/t/how-to-properly-backup-and-restore/12583/3


### info of borg structure
```
borg info ssh://username@your.domain.url:23/./path/to/backups/
```

### list all backups
```
borg list ssh://username@your.domain.url:23/./path/to/backups
```

### each backup
each backup listed in `borg list` hast property `::ARCHIVE` ex: `::_auto_someservice-2022-12-25_00:02`

### info of each backup
example
```
borg info ssh://username@your.domain.url:23/./path/to/backups::_auto_someservice-2022-12-25_00:02
```
### list each backup content
example will show all the content from a backup
```
borg list ssh://username@your.domain.url:23/./path/to/backups::_auto_someservice-2022-12-25_00:02
```

example grep some content from a backup so we can check that it is being processed
```
borg list ssh://username@your.domain.url:23/./path/to/backups::_auto_someservice-2022-12-25_00:02 | grep info.json
borg list ssh://username@your.domain.url:23/./path/to/backups::_auto_someservice-2022-12-25_00:02 | grep db.sql
borg list ssh://username@your.domain.url:23/./path/to/backups::_auto_someservice-2022-12-25_00:02 | grep dump.sql

```


## backup personal computer : local mounted drive

### mount bkp drive
```
mnt /dev/sdb1 /mnt/borgbkp
```

### get bort info
```
borg info /mnt/borgbkp/COMPUTERNAME/
```
(passphrase in the key ring)

### list borg backups
```
borg list /mnt/borgbkp/COMPUTERNAME
```

### create a new backup

excluding all `.cache` and `.tmp` files

 - see progress and stats and compress backup

```
borg create --stats --progress --compression zlib /mnt/borgbkp/COMPUTERNAME::COMPUTERNAMEbkp-20230602 /home  --exclude '/home/*/.cache' --exclude '.cache/*' --exclude '*.tmp'
```

 - see verbosed output

```
borg create -v --list /mnt/borgbkp/COMPUTERNAME::COMPUTERNAMEbkp-20230602 /home --exclude '/home/*/.cache' --exclude '.cache/*' --exclude '*.tmp'
```
