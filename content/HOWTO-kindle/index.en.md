---                                                                                                     
title: "Kindle Amazon alternatives" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto install alternativesto Amazon Kindle OS and free your kindle 4"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["kindle","jail break","e-book","reader","epub","mob","books"]
category: ["gnu/linux"]

lightgallery: true
---  

Tested on:
 - **[Model Kindle 4.1.0](https://wiki.mobileread.com/wiki/Kindle_4)**
 - **[Model Kindle 4.1.4](https://wiki.mobileread.com/wiki/Kindle_4)**
 
## Kual
Kindle Unified Application Launcher (v2.7) a.k.a. [Kual](https://wiki.mobileread.com/wiki/KUAL). Still active as for [november 2023](https://www.mobileread.com/forums/showthread.php?t=225030&page=77).

We will use the **Kindletool** to install it.

### Download kual and kindletool
from here [https://www.mobileread.com/forums/showthread.php?t=225030](https://www.mobileread.com/forums/showthread.php?t=225030)

### Install instructions for KUAL
https://www.mobileread.com/forums/showthread.php?t=203326

Welcome to Kindle Unified Application Launcher, your /kuːl/ friendly neighborhood Launcher .

What Does It Do?:

As the name implies, this is a Launcher application, in which anybody can plug into to provide new buttons and menus through extensions. Those buttons can run pretty much anything, and the backend is powerful enough to make menus dynamic, allowing developers to use KUAL as a configuration UI for their stuff! But the basic principle is: click a button to run stuff!

A number of hacks ship with a KUAL extension, which is why you'll often see KUAL listed as a required dependency .

Moreover, it's universal! It'll run on (pretty much) anything, as long as it's running FW >= 2.3! With a particular caveat on the KT2/KV/PW3, where some early firmware versions won't let you run it... And a fun twist on the KOA/KT3/KOA2, as well as on FW 5.9.x in general, more on that later...


## Duokan 
it is another different sofwtware that installs side by side with Kindle system.
It was packaged until 2019, no more development.

### About Duokan
[From mobileread](https://wiki.mobileread.com/wiki/Duokan): 
 
### Duokan download
 - [Reddit: where to download Duokan](https://www.reddit.com/r/kindle/comments/2j1amc/where_to_download_the_latest_duokan/)
 - [k4-xKindle_2014-06-12.36013.rtm](http://ota.read.duokan.com/mfsv2/file/s010/Hp01hWQyo0wb/bmbwsJXUYZP6TN/K4%28xKindle_2014-06-12.36013.rtm%29.zip)
   check download `md5sum cd0cd994a353596f203a6248c56c9add`

### Howto install Duokan:
Steps explained in Mobileread with no pictures but more accurate: https://wiki.mobileread.com/wiki/Duokan_2013_Installation

Explanation for an older Duokan firmware in arabic and english but works.
https://aboaladahem.wordpress.com/page/2/


#### steps:
 - Go to the home screen, press the menu button, and select "Settings". The version number is displayed at in the status bar at the bottom. 
 - Connect the Kindle to the PC with the USB cable. 
 - Unzip that archive, and copy the folder "DK_System" to the Kindle
 - Check your firmware version. Then, use either the folder "K4_4.0.0_to_4.0.1" or "K4_4.1.0_to_4.1.1"
 - Copy the directory "diagnostics_logs" and the files "data.tar.gz" and "ENABLE_DIAGS" from the subfolder to your Kindle
   [![Duokan install step 000 image](steps/img-000-80bc8-duokankindle4.jpg)](steps/img-000-80bc8-duokankindle4.jpg)
 - Unmount your Kindle drive (eject the drive on your PC) and disconnect the USB cable (for now)
 - Restart your Kindle 4: Go to home screen, press menu button, select "Settings", press menu button, select "Restart" *img-001*
  [![Duokan install step 001 image](steps/img-001-files-on-kindle21.png)](steps/img-001-files-on-kindle21.png)
 - kindle will start in debug mode img-002
   [![Duokan install step 002 image](steps/img-002-files-on-kindle31.png)](steps/img-002-files-on-kindle31.png)
 - Your Kindle will reboot into the diagnostics mode. 
 - with the 5 way button choose "D) Exit, Reboot or Disable Diags" *img-003*
   [![Duokan install step 003 image](steps/img-003-debug1.png)](steps/img-003-debug1.png)
 - now choose "D) Disable Diagnostics" *img-004*
   [![Duokan install step 004 image](steps/img-004-debug21.png)](steps/img-004-debug21.png)
 - then "Q) To continue". You might need to repeat this step several times. *img-005*
   [![Duokan install step 005 image](steps/img-005-debug3.png)](steps/img-005-debug3.png)
 - After rebooting, your Kindle should allow you to chose between Duokan and Kindle mode. Pick Duokan mode by pressing the leftmost button on your Kindle 4. 
 - The autoinstaller should start.  When it doesn’t just press the left key several times and repeat the procedure img-006
   [![Duokan install step 006 image](steps/img-006-debug4.png)](steps/img-006-debug4.png)
 - Just wait for the installer to finish and you should see a screen like this: img-007
   [![Duokan install step 007 image](steps/img-007-cd920-20140601_231556.jpg)](steps/img-007-cd920-20140601_231556.jpg)
 - this should be installed img-008
   [![Duokan install step 008 image](steps/img-008-6890d-20140601_231704.jpg)](steps/img-008-6890d-20140601_231704.jpg)

#### Change language
Duokan 2013 will display menus in simplified Chinese by default. Here's how you switch to your preferred language:
 - From the home screen, press the Menu button.
 - Click the last item on the list from the popup menu (ends with "设置").
   [![Duokan install step 009 image](steps/img-009-59190-20140601_231804.jpg)](steps/img-009-59190-20140601_231804.jpg)
 - Press the Next Page `>` button to get to the second page.
 - Select the 语言 (language) option, and select your chosen language, such as English.
   [![Duokan install step 010 image](steps/img-010-02e26-20140601_231738.jpg)](steps/img-010-02e26-20140601_231738.jpg)
 - Press Home, and Duokan will now display in your chosen language.

#### switch to kindle os
 - use menu key, select last option **System Settings**
   [![Duokan install step 011 image](steps/img-011-da981-20140601_234924.jpg)](steps/img-011-da981-20140601_234924.jpg)
 - select **Switch to Kindle** 
   [![Duokan install step 012 image](steps/img-012-0d267-20140601_234938.jpg)](steps/img-012-0d267-20140601_234938.jpg)

#### change screen saver
 - copy into the kindle file path `Kindle/DK_System/xKiindle/res/ScreenSaver` a grey scale `jpg` image.
   [![Duokan install step 013 image](steps/img-013-4731b-2-6-25570-03-15.jpg)](steps/img-013-4731b-2-6-25570-03-15.jpg)
 - this is how it shows
   [![Duokan install step 014 image](steps/img-014-fd2cb-20140601_235343.jpg)](steps/img-014-fd2cb-20140601_235343.jpg)

