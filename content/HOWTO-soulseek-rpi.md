## sistema slsk docker

Yo uso este docker-compose.yml
```
version: "2"
services:
  slskd:
    image: slskd/slskd:latest
    container_name: slskd
    ports:
      - "0.0.0.0:15000:5030"
      - "15001:5001"
      - "60000:50000"
    environment:
      - PUID=1000
      - PGID=1000
    volumes:
      - ./data:/app
      - ./music:/media/music
    restart: always
```

Antes de arrancarlo por primera vez:
```
$ mkdir data music
$ chmod 777 data music
```
Y luego ya:
```
$ docker-compose pull
$ docker-compose up -d
```
Y si quieres mirar los logs:
```
$ docker-compose logs -f
```
Y luego puedes entrar al soulseek via web:
```
http://localhost:15000
```
La documentación de esa imagen:

https://github.com/slskd/slskd

