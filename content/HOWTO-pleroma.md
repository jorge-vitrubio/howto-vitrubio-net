---
title: "Pleroma"
date: 2023-10-02
lastmod: 2023-11-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto maintain pleroma "
images: []
resources:
- name: "featured-image"
- src: "featured-image.png"
tags: ["pleroma","fediverse","postgresql"]
category: ["gnu/linux"]
lightgallery: true           
---

# Pleroma

## Database DB psql 

check the postgres HOWTO for understanding sizes and problems.

most commands come from the official pleroma docs
 - https://docs.pleroma.social/backend/administration/CLI_tasks/database/

execute bash as `pleroma` user
```bash
su pleroma -s $SHELL -lc "/PATH/TO/PLEROMA/bin/pleroma_ctl <COMMAND> <OPTIONS>"
```

### Prune old remote posts from the database
```bash
su pleroma -s $SHELL -lc "/var/www/pleroma/live/bin/pleroma_ctl database prune_objects --vacuum"
```
### Vacuum db
Analyze first
```bash
su pleroma -s $SHELL -lc "/var/www/pleroma/live/bin/pleroma_ctl database vacuum analyze"
```
Run a `vacuum` after
```bash
su pleroma -s $SHELL -lc "/var/www/pleroma/live/bin/pleroma_ctl database vacuum full"
```

