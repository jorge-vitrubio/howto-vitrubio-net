---                                                                                                     
title: "systemctl commands" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto use and check systemctl"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["systemctl","services","debian","sysadmin"]
category: ["gnu/linux","sysadmin"]

lightgallery: true
---  

## check status
all services status
```
systemctl status
```
for one service
```
systemctl status <service_name>
```

## check failed
```
systemctl --failed
```

also by state
```
systemctl list-units --state failed
```

## manage service
stop a service
```
systemctl stop <service_name>
```
start a service
```
systemctl start <service_name>
```
restart a service
```
systemctl restart <service_name>
```
reload a service with out stop/start/restart
```
systemctl reload <service_name>
```

