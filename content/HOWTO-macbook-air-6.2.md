---                                                                                                     
title: "Installing GNU/linux in MacBook Air 6.2" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto install GNU/Linux in a MacBook Air 6.2"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["install","linux","mac","macbook","debian"]
category: ["gnu/linux"]

lightgallery: true
---  



### No boot loader installed

`/vmlinuz` kernel on partition `/dev/sdc5` and `root=/dev/sdc5` `quiet` passed as kernel argument.
