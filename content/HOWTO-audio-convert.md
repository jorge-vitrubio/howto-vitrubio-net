---                                                                                                     
title: "convert audio files - flac ogg mp3 m4a" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto convert audio files from different formats to different formats"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["audio","convert","mp3","flac","ogg","m4a"]
category: ["gnu-linux"]

lightgallery: true
---  

## Convert files from different audio formats

Readin [Baeldung](https://www.baeldung.com/linux/flac-mp3-cli-conversion) and [Lewis Diamond blog](https://lewisdiamond.blogspot.com/2012/01/converting-flac-to-mp3.html) I have come up to this conclusion


### ffmpeg

ffmpeg will parse conversion to an external interpreter.

- Best quality: `ffmpeg -i sample1.flac -q:a 0 -map a output1.mp3`
- Mid quality: `ffmpeg -i sample1.flac -acodec libmp3lame -ab 320k output1.mp3`
- Smallest size: `ffmpeg -i sample1.flac -acodec libmp3lame -ab 128k output1.mp3`

### lame

will work faster, it does not need an external converter, does it directly.

- Best quality: `lame --preset extreme sample1.flac output2.mp3`
- Mid quality: `lame -b 320 sample1.flac output2.mp3`
- Smallest size: `lame sample1.flac output2.mp3`

### batch

Using `find -name "*.AUDIOFORMAT"` we can parse the ressult to `-exec` and do audio conversion as noted before, depends on your preferences, for example:

- ffmpeg medium quallity adding `.mp3` to the name preserves metadata: 
  `find -name "*.flac" -exec ffmpeg -i {} -acodec libmp3lame -ab 320k {}.mp3 \;`
- ffmpeg better quality adding `.mp3` to the name preserves metadata: 
  `find -name "*.flac" -exec ffmpeg -i {} -q:a 0 -map a {}.mp3 \;`
- lame adding `.mp3` to the name, does not preserve metadata:
  `find . -name "*.flac" -exec lame --preset extreme {} {}.mp3 \;`

### best choice
- flac to mp3
```
find -name "*.flac" -exec bash -c 'ffmpeg -i "{}" -q:a 0 -map a "${0/.flac}.mp3"' {}  \;
```
- m4a to mp3
```
find -name "*.m4a" -exec bash -c 'ffmpeg -i "{}" -q:a 0 -map a "${0/.m4a}.mp3"' {}  \;
```
