---
title: "virsh a libvirt client" 
date: 2024-02-22
lastmod: 2024-02-22
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto libvirt virtualice using kvm images controlled by virsh"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["virsh","kvm","virtual","libvirt"]
category: [""]

lightgallery: true
--- 

## kvm cheat sheet
https://blog.programster.org/kvm-cheatsheet

## create and manage kvm
https://www.ostechnix.com/setup-headless-virtualization-server-using-kvm-ubuntu/
https://linuxconfig.org/how-to-create-and-manage-kvm-virtual-machines-from-cli

#### command
```
sudo virsh net-start default
```
```
virt-install --name=localdocker \
--vcpus=1 \
--cpu host \
--hvm \
--memory=1024 \
--cdrom=/home/vitrubio/Documents/tech/debian-installer/debian-10.0.0-amd64-netinst.iso \
--disk path=/home/vitrubio/Documents/virtualmachines/localdocker.qcow2,size=8 \
--os-variant=debiantesting \
--graphics vnc
```

## remove kvm frmo virsh
https://www.cyberciti.biz/faq/howto-linux-delete-a-running-vm-guest-on-kvm/

```
virsh list --all
```
to know how it is build
```
virsh dumpxml VM_NAME
```
to know where it is placed the source file
```
virsh dumpxml --domain VM_NAME | grep 'source file'

<source file='/path/to/the/VM_NAME.qcow2' />
```
Shut down the VM
```
virsh destroy VM_NAME
```
Delete tehe virtual manchine
```
virsh undefine VM_NAMiE
rm -rf /paht/to/the/VM_NAME.qcow2
```

### define new vm

locate the xml file
```
sudo find / |grep VM_NAME | grep xml
```
the file has the rout to the VM_NAME.qcow2 and we can change it to a new place
`<source file='/path/to/the/VM_NAME.qcow2' />` using
```
virsh edit VM_NAME
```

then we can define the new machie
``` 
virsh define /path/to/the/VM_NAME.xml
```



