---
title: "execute .AppImage" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto install AppImage in linux"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["appimage","linux"]
category: ["gnu/linux"]

lightgallery: true
---

# AppImage how to use

in linux we can follow theese steps: 
 - https://www.howtogeek.com/827849/how-to-use-appimages-on-linux/

give executing permissions
```
chmod +x osmc-installer.AppImage
```
execite file
```
./osmc-installer.AppImage
```

