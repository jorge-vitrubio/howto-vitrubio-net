# vim editor

## howto personalize
edit `~/.vimrc` to add options
```
" edited on 2022 12 29
" https://www.freecodecamp.org/news/vimrc-configuration-guide-customize-your-vim-editor/
" apply changes do
" :soruce ~/.vimrc
"
set encoding=utf-8
set tabstop=2
set softtabstop=2
set shiftwidth=2
set expandtab
set autoindent
set number
set cursorline
set cursorcolumn
set showcmd
set showmode
set hlsearch
set incsearch
set showmatch
```
## plugins

### folding lines

```

" https://www.freecodecamp.org/news/vimrc-configuration-guide-customize-your-vim-editor#how-to-fold-long-fi    les-in-vim
"
" PLUGINS ---------------------------------------------------------------- {{{

" Plugin code goes here.

" }}}


" MAPPINGS --------------------------------------------------------------- {{{

" Mappings code goes here.

" }}}


" VIMSCRIPT -------------------------------------------------------------- {{{

" This will enable code folding.
" Use the marker method of folding.
augroup filetype_vim
    autocmd!
    autocmd FileType vim setlocal foldmethod=marker
augroup END

" More Vimscripts code goes here.

" }}}


" STATUS LINE ------------------------------------------------------------ {{{

" Status bar code goes here.

" }}}

```

## commands

### auto indent
do this commands `esc` then `gg=G`

## search replace regex

### between brackets special characters

having `<foo what ever s&imbol some 234 and something>` searching from firts `<` until first `>` 

need to match anything using `*` except the first `>` sot it will stop, and then find the `>`

```
/\<foo[^>]*\>
```
this means
 - `/` seach
 - starting with`\<foo\`
 - anything but *>* `[^>]*` so it finds `>` everything but the last `>`
 - then find next *>* using `\>`


### delete everythin between brackest and them too

search the previous metod and repace with nothing
```
:%s/\<foo[^>]*\>//g
```
