---
title: "About"
date: 2023-10-14
lastmod: 2023-10-14
draft: false
description: "About this howto page"


linkTitle: "About these howto's"
---


This web was rendered by [Hugo](https://gohugo.io/) a static site generator.

So far uses the [Dark Theme Editor](https://github.com/JingWangTW/dark-theme-editor).

The result has been uploaded to the server as static content.
