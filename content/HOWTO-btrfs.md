---                                                                                                                                                         
title: "BTRFS raid filesystem" 
date: 2024-09-03
lastmod: 2024-09-03
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto use and repair BTRFS files sistems"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["filesystem","raid"]
category: ["gnu/linux"]
 
lightgallery: true
---  

## info

command `btrfs fi show` 

example

```
:~# btrfs fi show
Label: none  uuid: 711ddb53-318b-43d6-91cd-b18c8983e886
	Total devices 2 FS bytes used 3.34GiB
	devid    1 size 46.57GiB used 6.03GiB path /dev/sda3
	devid    2 size 46.57GiB used 6.03GiB path /dev/sdb3

Label: none  uuid: 00c63666-d63d-4c08-8d18-81b1c33bbef7
	Total devices 2 FS bytes used 113.27MiB
	devid    1 size 1.86GiB used 672.00MiB path /dev/sda1
	devid    2 size 1.86GiB used 672.00MiB path /dev/sdb1

Label: none  uuid: 508358ca-5cbe-47db-befb-3656307883ea
	Total devices 2 FS bytes used 435.65GiB
	devid    1 size 1.77TiB used 437.06GiB path /dev/sda4
	devid    2 size 1.77TiB used 437.06GiB path /dev/sdb4
```
## check

## repair scrub

`btrfs scrub start -Bq <mountpoint>`
## rescue

## other
