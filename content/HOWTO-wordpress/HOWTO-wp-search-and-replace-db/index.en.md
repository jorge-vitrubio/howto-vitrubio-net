---                                                                                                     
title: "WordPress: database search and replace plugin"
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["wordpress","database","db","plugin","migrate","search","replace"]
category: ["wordpress"]

lightgallery: true
--- 

source: <https://interconnectit.com/products/search-and-replace-for-wordpress-databases/>

source: <https://github.com/interconnectit/Search-Replace-DB>

## Search and Replace DB

This plugin helps you migrate a WordPress database by searching and replacing fields in the db.

steps:
 1. download the plugin from the github releases <https://github.com/interconnectit/Search-Replace-DB/releases>
 2. unpack it inside the wordpress main directory, where `wp-config.php` is
 3. give [proper permissions]({{< ref "HOWTO-wp-files-setup" >}}) to the *Search-Replace-DB-master* directory
 4. optional: change directory name to something easier like *SRDB* `mv Search-Replace-DB-master SRDB`
 5. visit the link: <https://YOUR.WORDPRESS.URL/Search-Replace-DB-master>
 6. have handy:
    * database name
    * database user
    * database password
    * database location (URL or localhost)
    * database port
 7. test the connection to the database
 8. search <https://OLD.SITE.URL> replace <https://NEW.SITE.URL>
 9. do a dry run first (wise)
 10. do the run and change any other parameters like:
    * search <http://OLD.SITE.URL> replace <http://NEW.SITE.URL>
    * maybe the location of `uploads`: `/old/path/wp-content/uploads` to `/new/path/wp-conten/uploads`
    * ...
 11. **DELETE** the *Search-Replace-DB-master* directory **IMPORTANT**
 11. **DELETE** the *Search-Replace-DB-master* directory **IMPORTANT**
 11. **DELETE** the *Search-Replace-DB-master* directory **IMPORTANT**
 12. and yes...
 13. **DELETE** the *Search-Replace-DB-master* directory **IMPORTANT**
 
Very important to not leave this code out in the public url.
