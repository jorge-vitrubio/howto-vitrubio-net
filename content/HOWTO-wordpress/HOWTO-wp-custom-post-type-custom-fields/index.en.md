---                                                                                                     
title: "WordPress: custom postypes and custom fields"                                                                                               
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["wordpress","custom post-type","custom field","php"]
category: ["wordpress"]
         
lightgallery: true
---  

#Custom Fields

## clean up custom fields orphaned


mysql -u DBUSER -p DBNAME
```
SHOW DATABASES;
USE dbname;
SHOW TABLES;
SHOW COLUMNS FROM wp_postmeta;
SHOW FULL COLUMNS FROM wp_postmeta;
```
show contents
```
SELECT * FROM wp_postmeta WHERE meta_value;
```
```
SELECT * FROM wp_postmeta WHERE meta_key LIKE 'stringfoobar%';
```
delete the info not needed stringfoobar
stringfoobar%  starts with
%stringfoobar  ends with
%stringfoobar%  has in the middle
```
DELETE FROM wp_postmeta WHERE meta_key LIKE '%stringfoobar%';
```

### if custom type change
replace 'foo-old' for 'bar-new' in TABLE COLUMNS
```
UPDATE wp_posts SET post_type = 'bar-new' WHERE post_type = 'foo-old';
```
replace 'foo_bar' for 'bar-foo' in TABLE COLUMNS
```
UPDATE wp_posts SET post_type = replace(post_type,'foo_bar','bar-foo');
UPDATE wp_posts SET guid = replace(guid,'foo_bar','bar-foo');
```
