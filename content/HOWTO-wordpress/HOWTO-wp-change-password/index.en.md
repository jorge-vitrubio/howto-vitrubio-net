---                                                                                                     
title: "WordPress: reset password" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto reset admin or user password in WordPress"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["wordpress","password","reset","linux"]
category: ["wordpress"]
 
lightgallery: true
---
 


As read from here:
https://wordpress.org/documentation/article/reset-your-password/


## Through MySQL Command Line

### generate md5 for new password
in another computer:
 - http://www.miraclesalad.com/webtools/md5.php
 - or using Pyton
 - or unix/linux:
    -  Create a file called wp.txt, containing nothing but the new password.
    -  `tr -d ‘\r\n’ < wp.txt | md5sum | tr -d ‘ -‘`
    -  keep the MD5 string as MD5-STRING-YOU-MADE will be needed
    -  rm wp.txt

#### access docker db
```
docker exec -it ofisuportxxp-wp-db-localdkr bash -l
```
#### change the password
``` 
mysql -u root -p

USE name-of-db;
SHOW TABLES;
SELECT ID, user_login,user_pass FROM name-of-table;
UPDATE name-of-table SET user_pass="MD5-STRING-YOU-MADE" WHERE ID = "id-you-saw";
```

## Through FTP functions.php

edit `functions.php` adding this at the begining

```
wp_set_password( 'my_new_password', user_id );
```
 - then try to login -> will not succeed
 - then erase this line `wp_set_password( 'my_new_password', user_id );`
 - go to login with new password
