---                                                                                                     
title: "WordPress: files permissions"
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto setup file permissions and ownership for wordpress in a web server"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["wordpress","files","sysadmin","permissions"]
category: ["wordpress"]

lightgallery: true
--- 

source: https://stackoverflow.com/questions/18352682/correct-file-permissions-for-wordpress#23755604

## Ownership and Permissions

### Let webserver be owner
```
chown www-data:www-data  -R *
```

### Directory permissions rwxr-xr-x
```
find . -type d -exec chmod 755 {} \;
```

### File permissions rw-r--r--
```
find . -type f -exec chmod 644 {} \;  
```
