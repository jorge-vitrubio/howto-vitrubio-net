---                                                                                                      
title: "WordPress docker and migrating"
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["wordpress","docker","mysql","mariadb","migrating"]
category: ["wordpress"]

lightgallery: true
---  

Before this you might need to read the [HOWTO Docker]({{< ref "HOWTO-docker" >}}).


This howto explains howto install a docker with WordPress and how to migrate an existing WordPress db and files into the docker so local development can be acheived.
## Docker WordPress

https://hub.docker.com/_/wordpress/

first install the latest the docker `docker pull wordpress`

other versions here: https://hub.docker.com/_/wordpress?tab=tags

for example: v5 and php 7 `docker pull wordpress:5.9.3-php7.4-fpm`

create a directory, move inside and declare the `docker-compose.yml` containing this:

```      
# docker-compose.yml
#        
version: '3.3'
services:
  db:    
    container_name: 'PROJECTNAME-wp-db-localdkr'
    image: 'mysql:5.7'
    volumes:
      - './data/mysql:/var/lib/mysql'
    ports:
      - 18766:3306
    environment:
      MYSQL_ROOT_PASSWORD: PROJECTNAME_dockerwp_ROOTPWD
      MYSQL_DATABASE: PROJECTNAME_dockerwp_DB
      MYSQL_USER: PROJECTNAME_dockerwp_USER
      MYSQL_PASSWORD: PROJECTNAME_dockerwp_PWD
  wordpress:
    container_name: 'PROJ
    depends_on:
      - db
    image: 'wordpress:latest'
    ports:
      - '80:80'
    environment:
      WORDPRESS_DB_HOST: 'db:3306'
      WORDPRESS_DB_NAME: PROJECTNAME_dockerwp_DB
      WORDPRESS_DB_USER: PROJECTNAME_dockerwp_USER
      WORDPRESS_DB_PASSWORD: PROJECTNAME_dockerwp_PWD
    volumes:
      #- "./wordpress:/var/www/html"
      - "./themes:/var/www/html/wp-content/themes"
      - "./plugins:/var/www/html/wp-content/plugins"
      - "./uploads:/var/www/html/wp-content/uploads"
      - "./uploads.ini:/usr/local/etc/php/conf.d/uploads.ini"
```      
create the directories else they will be created with `root` privileges.
```      
mkdir themes
mkdir plugins
mkdir uploads
```      

create `uploads.ini` with content
```      
file_uploads = On
memory_limit = 64M
upload_max_filesize = 64M
post_max_size = 64M
max_execution_time = 600
```      
then run `docker-compose up`

it will run wordpress in http://localhost

if want to stop pres `ctr + c`

if want to run in the backgroudn all the time as a daemon
`docker-compose up -d`

#### migrate site from another server into docker wp
##### import db into docker

 - get a sql dump from the original site.
     `mysqldump -u DBUSERexternal -p DBNAMEexternal > DBNAMEexternal.yymmdd.sql`
 - place it into `data/mysql` from the host machine
 - attach to docker machine
    `docker ps`
    `docker exec -it <container_name> bash -l`
 - inside the database docker `cd /var/lib/mysql/`
 - backup / dump the existing db 
     `mysqldump -u DBUSERdocker -p DBNAMEdocker > DBNAMEdocker.yymmdd.sql`
 - parse the original db `mysql -u DBUSERdocker -p DBNAMEdocker < DBNAME.external.yymmdd.sql`

##### modify the db
##### Using mysql commands
change values using mysql
https://wpbeaches.com/updating-wordpress-mysql-database-after-moving-to-a-new-url/
```
UPDATE wp_options SET option_value = replace(option_value, 'http://www.oldurl', 'http://www.newurl') WHERE option_name = 'home' OR option_name = 'siteurl';

UPDATE wp_posts SET guid = replace(guid, 'http://www.oldurl','http://www.newurl');

UPDATE wp_posts SET post_content = replace(post_content, 'http://www.oldurl', 'http://www.newurl');

UPDATE wp_postmeta SET meta_value = replace(meta_value,'http://www.oldurl','http://www.newurl');
```
after that also be aware of serialized daga with php and db, then use this
https://interconnectit.com/products/search-and-replace-for-wordpress-databases/


##### Usin Search And Replace DB
Download the 
 - https://interconnectit.com/products/search-and-replace-for-wordpress-databases/
 - https://github.com/interconnectit/Search-Replace-DB

Place the zip or folder into your WP docker file system under `/var/www/html`, to do so:
 - Read the public accessible host machine directories from the `docker-compose.yml`, place 
 - place the Search and Replace DB files into one of them, check ownership
 - attach to the docker by i
    `docker exec -it <container_name> bash -l`
 - move extracted files to the `/var/www/html/SRDB`
 - visit url: http://localhost/SRDB

Proceed as usual migrating sites: 
 - https://ORIGINALURL -> http://localhost
 - http://ORIGINALURL -> http://localhost
 - DBUSERdocker, DBNAMEdocker,DBPASSWORDdocker,DBHOSTdocker,DBPORTdocker
    the info is defined in the dockerfile:
    - DBHOSTdocker `db`
    - DBPORTdocker `3306`

test connection and you are good to go.
