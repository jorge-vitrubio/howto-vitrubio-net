---                                                                                                     
title: "WordPress: clean up DB custom fields orphaned"                                                                                               
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["wordpress","mysql","mariadb","custom fields"]
category: ["wordpress"]
         
lightgallery: true
---  

## attach to mysql

```
mysql -u DBUSER -p DBNAME
```
### check databases, tables and columns
```
SHOW DATABASES;
USE dbname;
SHOW TABLES;
SHOW COLUMNS FROM wp_postmeta;
SHOW FULL COLUMNS FROM wp_postmeta;
```
### show db contents
```
SELECT * FROM wp_postmeta WHERE meta_value;
```
```
SELECT * FROM wp_postmeta WHERE meta_key LIKE 'stringfoobar%';
```

### delete from db

delete the info not needed stringfoobar
 - `stringfoobar%`  starts with
 - `%stringfoobar`  ends with
 - `%stringfoobar%`  has in the middle

```
DELETE FROM wp_postmeta WHERE meta_key LIKE '%stringfoobar%';
```
### replace from db
replace `foo-old` for `bar-new` in TABLE COLUMNS

```
UPDATE wp_posts SET post_type = 'bar-new' WHERE post_type = 'foo-old';
```

replace `foo_bar` for `bar-foo` in TABLE COLUMNS
```
UPDATE wp_posts SET post_type = replace(post_type,'foo_bar','bar-foo');
UPDATE wp_posts SET guid = replace(guid,'foo_bar','bar-foo');
```
