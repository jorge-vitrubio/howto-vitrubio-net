---                                                                                                     
title: "ssh tunnel proxy" 
date: 2023-10-02
lastmod: 2023-10-10
draft: false
author: "Jorge"
authorLink: "https://vitrubio.net"
description: "howto do an ssh tunnel for web traffic as a proxy"
images: []
resources:
- name: "featured-image"
  src: "featured-image.png"
tags: ["ssh","tunnel","proxy","ip"]
category: ["gnu/linux"]

lightgallery: true
---  

## steps

### do ssh
open terminal and type
```
ssh -D 8080  myserver.domain.tdl
```
you can use your domain.tdl or the server IP address


### browser
open the web browser, in settings configure: 
```
proxy socks5 localhost:8080
```

### check connection
in terminal
```
curls ifconfig.co
```

## issues

If `channel 4: open failed: connect failed: open failed` you can ignore this. JS does call too many times so the tunnel complains.

If connection goes down, just restart it `ctr`+`c` and do again `ssh -D 8080  myserver.domain.tdl`
